#!/usr/bin/python

import sys
import socket
import json

#-----------------------------------------------------------------------------

if len(sys.argv) < 4 or sys.argv[1] in ["-h", "--help"]:
  print "Usage: %s host:port channel_name JSON" % (sys.argv[0],)
  sys.exit()

#-----------------------------------------------------------------------------

address, port = sys.argv[1].split(":")
port = int(port)

channel = sys.argv[2]

message = json.loads(sys.argv[3]) # I'll dump it back to JSON just after this

#-----------------------------------------------------------------------------

# NOTE: socket.recv(4096) is not actually a correct way of reading a single
# line from socket

streem = socket.socket()
streem.connect((address, port))

streem.send(json.dumps({'register': channel}) + '\n')
reply = json.loads(streem.recv(4096))
if reply['status'] != 'ok':
  print "Error when registering channel: %s" % (reply['message'],)
  sys.exit(1)

streem.send(json.dumps({'submit': message}) + '\n')
reply = json.loads(streem.recv(4096))
if reply['status'] != 'ok':
  print "Error when sending: %s" % (reply['message'],)
  sys.exit(1)

#-----------------------------------------------------------------------------
# vim:ft=python
