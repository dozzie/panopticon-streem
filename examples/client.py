#!/usr/bin/python

import sys
import socket
import json

#-----------------------------------------------------------------------------

if len(sys.argv) < 3 or sys.argv[1] in ["-h", "--help"]:
  print "Usage: %s host:port { --query query | channel_name }" % (sys.argv[0],)
  sys.exit()

#-----------------------------------------------------------------------------

address, port = sys.argv[1].split(":")
port = int(port)

if sys.argv[2] == '--query':
  query = sys.argv[3]
  channel = None
elif sys.argv[2].startswith('--query='):
  query = sys.argv[2][8:]
  channel = None
else:
  query = None
  channel = sys.argv[2]

#-----------------------------------------------------------------------------

try:
  streem = socket.socket()
  streem.connect((address, port))

  if query is not None:
    request = json.dumps({'query': query})
    streem.send(request + '\n')
  elif channel is not None:
    request = json.dumps({'subscribe': channel})
    streem.send(request + '\n')

  # NOTE: socket.recv(4096) is not actually a correct way of reading a single
  # line from socket

  # ignore this one; it's {"status":"ok","message":"..."}
  reply = streem.recv(4096)

  while True:
    line = streem.recv(4096)
    reply = json.loads(line)
    if 'message' in reply:
      print json.dumps(reply['message'])

except KeyboardInterrupt:
  pass

#-----------------------------------------------------------------------------
# vim:ft=python
