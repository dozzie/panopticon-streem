#!/usr/bin/escript
%%! -pz streem/ebin
%%----------------------------------------------------------------------------

main([])     -> main(["--help"]);
main(["-h"]) -> main(["--help"]);
main(["-?"]) -> main(["--help"]);
main(["--help"]) ->
  io:fwrite("Usage:~n  ~s host port~n  ~s port~n",
            [escript:script_name(), escript:script_name()]),
  ok;

main([PortStr]) ->
  start(any, list_to_integer(PortStr)),
  timer:sleep(infinity);
main([Host, PortStr]) ->
  start(Host, list_to_integer(PortStr)),
  timer:sleep(infinity);

main(_) ->
  main("--help").

%%----------------------------------------------------------------------------

start(Host, Port) ->
  Addr = case Host of
    any -> any;
    _ ->
      {ok, HAddr} = inet:getaddr(Host, inet),
      HAddr
  end,
  application:load(streem),
  application:set_env(streem, listen, [{Addr, Port}]),
  ok = start_rec(streem),
  io:fwrite("Listening on ~s:~B~n", [Host, Port]),
  ok.

start_rec(App) ->
  case application:start(App) of
    {error, {not_started, AppDep}} ->
      ok = start_rec(AppDep),
      start_rec(App);
    {error, {already_started, App}} ->
      io:fwrite("+ ~p was already started~n", [App]),
      ok;
    ok ->
      io:fwrite("+ ~p started~n", [App]),
      ok;
    {error, Reason} ->
      io:fwrite("! ~p not started: ~p~n", [App, Reason]),
      {error, Reason}
  end.

%%----------------------------------------------------------------------------
%% vim:ft=erlang
