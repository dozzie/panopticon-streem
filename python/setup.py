#!/usr/bin/python

from setuptools import setup, find_packages

setup(
  name         = 'streem',
  version      = '0.0.1',
  description  = "Streem client",
  packages     = find_packages("lib"),
  package_dir  = { "": "lib" },
)
