#!/usr/bin/python
#
# SJCP (Streem JSON Client Protocol) client class.
#
#-----------------------------------------------------------------------------

import tcpline
import json

#-----------------------------------------------------------------------------

class ProtocolError(RuntimeError):
  pass

#-----------------------------------------------------------------------------

class SJCP:
  def __init__(self, host, port):
    self.host = host
    self.port = port
    self.conn = tcpline.TCPLine(host, port)
    self.buf = []

  #---------------------------------------------------------
  # low level details {{{

  def command(self, command):
    self.conn.writeline(json.dumps(command))
    while True:
      reply = self._raw_receive()
      if 'status' in reply and 'message' in reply:
        return reply
      else:
        # not a reply to this command, so it's an incoming message; save for
        # later retrieval and try again
        self.buf.append(reply)
        continue

  def _raw_receive(self):
    line = self.conn.readline()
    if line is None:
      return None
    else:
      return json.loads(line)

  # }}}
  #---------------------------------------------------------

  def receive(self):
    if len(self.buf) > 0:
      result = self.buf.pop(0)
    else:
      result = self._raw_receive()
    if result is not None:
      result = result['message']
    return result

  def submit(self, message):
    reply = self.command({"submit": message})
    if reply['status'] != 'ok':
      raise ProtocolError(reply['message'])

  def subscribe(self, channel):
    reply = self.command({"subscribe": channel})
    if reply['status'] != 'ok':
      raise ProtocolError(reply['message'])

  def unsubscribe(self, channel):
    reply = self.command({"unsubscribe": channel})
    if reply['status'] != 'ok':
      raise ProtocolError(reply['message'])

  def register(self, channel):
    reply = self.command({"register": channel})
    if reply['status'] != 'ok':
      raise ProtocolError(reply['message'])

  def unregister(self, channel):
    reply = self.command({"unregister": channel})
    if reply['status'] != 'ok':
      raise ProtocolError(reply['message'])

#-----------------------------------------------------------------------------
# vim:ft=python:foldmethod=marker
