#!/usr/bin/python

import socket

#-----------------------------------------------------------------------------

class TCPLine:
  def __init__(self, host, port):
    self.host = host
    self.port = port
    self.conn = socket.socket()
    self.conn.connect((host, port))
    self.buf = []

  def close(self):
    if self.conn is not None:
      self.conn.close()
      self.conn = None

  def is_eof(self):
    return (self.conn is None)

  def write(self, data):
    if self.conn is None:
      raise IOError('connection to %s:%d closed' % (self.host, self.port))

    self.conn.send(data)

  def writeline(self, line):
    if self.conn is None:
      raise IOError('connection to %s:%d closed' % (self.host, self.port))

    if line.endswith("\n"):
      self.conn.send(line)
    else:
      self.conn.send(line + "\n")

  def readline(self):
    if self.conn is None:
      raise IOError('connection to %s:%d closed' % (self.host, self.port))

    if len(self.buf) > 0:
      # if there is anything left after previous call, it's a single element
      nl = self.buf[0].find("\n")
      if nl > -1:
        # if it already contains newline, strip the part up to newline and
        # return it to the caller
        line = self.buf[0][0:nl+1]
        self.buf[0] = self.buf[0][nl+1:]
        return line

    while True:
      # try reading a single line
      msg = self.conn.recv(4096)

      # EOF
      if msg == '':
        line = ''.join(self.buf)
        del self.buf[:]
        self.close()
        return ''.join(self.buf)

      nl = msg.find("\n")
      if nl == -1:
        # no newline in this part: append the part and try again
        self.buf.append(msg)
        continue
      else:
        # if there is a newline, join it with all the parts from self.buf
        # (none of them should contain newline)
        line = ''.join(self.buf) + msg[0:nl+1]

        # now remember everything after first newline as the only element in
        # self.buf (if non-empty)
        del self.buf[:]
        rest = msg[nl+1:]
        if rest != '':
          self.buf.append(rest)

        return line

#-----------------------------------------------------------------------------
# vim:ft=python
