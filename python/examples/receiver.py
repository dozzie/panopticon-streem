#!/usr/bin/python

import sys
import streem

#-----------------------------------------------------------------------------

if len(sys.argv) < 3 or sys.argv in ('-h', '--help'):
  print "Usage: %s host:port channel" % (sys.argv[0],)
  sys.exit()

(host, port) = sys.argv[1].split(':')
port = int(port)
channel = sys.argv[2]

#-----------------------------------------------------------------------------

s = streem.Streem(host, port)
s.subscribe(channel)
try:
  while True:
    msg = s.receive()
    if msg is None:
      break
    print "received: %s" % (msg,)
except KeyboardInterrupt:
  pass

#-----------------------------------------------------------------------------
# vim:ft=python
