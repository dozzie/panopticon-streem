#!/usr/bin/python

import sys
import json
import streem

#-----------------------------------------------------------------------------

if len(sys.argv) < 3 or sys.argv in ('-h', '--help'):
  print "Usage:"
  print "  %s host:port channel json_message" % (sys.argv[0],)
  print "  %s host:port channel < messages.json" % (sys.argv[0],)
  sys.exit()

(host, port) = sys.argv[1].split(':')
port = int(port)
channel = sys.argv[2]

if len(sys.argv) == 4:
  message = json.loads(sys.argv[3])
else:
  message = None

#-----------------------------------------------------------------------------

s = streem.Streem(host, port)
s.register(channel)

if message is not None:
  s.submit(message)
else:
  try:
    while True:
      line = sys.stdin.readline()
      if line == '':
        break
      s.submit(json.loads(line))
  except KeyboardInterrupt:
    pass

#-----------------------------------------------------------------------------
# vim:ft=python
