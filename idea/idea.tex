\documentclass[10pt]{article}

%-----------------------------------------------------------------------------
% packages {{{

% language settings
\usepackage[polish,english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[OT4]{fontenc}

% page and paragraph layout
\usepackage{a4wide}
\usepackage{indentfirst}

% formatting helpers
\usepackage{enumitem}       % \begin{itemize}[noitemsep]
\usepackage[normalem]{ulem} % \sout{}, strike through

% additional environments
\usepackage{listings}
\usepackage{color}
\usepackage{tikz}
\usetikzlibrary{calc,positioning}
%\usepackage{graphicx}

% testing
\usepackage{lipsum}

% }}}
%-----------------------------------------------------------------------------
% commands and definitions {{{

\newcommand{\func}[1]{\texttt{#1()}}
\newcommand{\term}[1]{\textit{#1}}
\newcommand{\toolbox}[1]{\textit{#1}}

\renewcommand{\labelitemi}{\textbullet} % it's default
\renewcommand{\labelitemii}{$\circ$}
\renewcommand{\labelitemiii}{--}

\definecolor{kword}{rgb}{0.70,0.2,0.2}
\definecolor{string}{rgb}{1,0,1}
\definecolor{comment}{gray}{0.50}

% \begin{lstlisting}[language=erlang]
% ...
% \end{lstlisting}
\lstset{
    %language=erlang,
    basicstyle=\ttfamily\small,
    prebreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
    %frame=lines,
    frame=shadowbox,
    aboveskip=0.5cm, belowskip=0.5cm,
    keywordstyle=\color{kword}\bfseries,
    stringstyle=\color{string},
    commentstyle=\color{comment}\itshape,
    %numbers=left,
    captionpos=t,
    %escapeinside={\%*}{*)}
}

% }}}
%-----------------------------------------------------------------------------

\begin{document}

\section{Application purpose and description}

Streem is a stream database, meant to process messages as they from different
sources. Each incoming message can be processed by one or more queries, which
are called ``streams''. Streams may filter messages as they flow through,
alter them, group them, calculate statistics on them and output the results
for other streams to consume. Clients can submit a query, subscribe to
already-defined stream or submit new messages.

\subsection{Vocabulary}

%-----------------------------------------------------------
% vocabulary {{{

\begin{description}
  \item[stream] Query expressing what to do with messages. A stream consists
    of one or more \term{operations}.
  \item[message] Data structure of the same data model as JSON documents.
    Typically, a message is a hash.
  \item[publish] One of the stream's possible outputs. Other streams can
    take the current stream as their \term{message source}.
  \item[subscribe] Stream that takes as its input messages produced by some
    other stream is told to ``subscribe'' that stream.
  \item[feed] Stream's output may be directed to some other stream's input,
    e.g. to replay some messages. This is called ``feeding'' the latter
    stream.
  \item[operation] Primitive for building \term{streams}. Operation does
    something to a message, to group of messages or it simply rejects the
    message. Operation has a single input and two outputs: with the
    operation's results and with rejected messages.
  \item[stateless operation] Operation that works on a single message at
    a time. Examples of such operations are \term{map} and \term{filter} known
    from functional programming languages (\term{filter} is called here
    \term{grep}, after Perl).
  \item[windowed operation] Operation that takes several messages at a time
    and produces a single message based on them. Example is average on last
    5 messages.
  \item[timed operation] Operation that takes all the messages that came in
    recent time window. Similar to \term{windowed operation}, but instead of
    working on fixed number of messages, it uses a time window. \\
    Some notes:
    \begin{itemize}[noitemsep]
      \item what to do when no messages hit the time window?
      \item replayed stream --- on which attribute to calculate time window?
        input time (real)? timestamp from message?
    \end{itemize}
  \item[FSM operation] Operation defined as a finite-state machine. Such
    operation creates FSMs dynamically, with FSM being identified (referred
    to) by set of values from a message.
    \begin{itemize}[noitemsep]
      \item FSM can produce message on transition
      \item FSM can produce message on message on input (no transition)
      \item FSM can produce messages in regular intervals
      \item FSM can reset on timeout
    \end{itemize}
  \item[message source] Description of input for the stream. A stream may have
    one or more sources (\textit{or none? feed-only?}). A source can produce
    messages continuously, like messages from external systems coming in real
    time, or just replay messages from some storage, like database.
  \item[continuous source] Previously called ``real-time source''.
  \item[replaying source] Previously called ``database source''.
  \item[Spidle] SPDL or SPiDLe stands for Stream and Pipe Definition Language.
    Given how hard is to write SPiDLe, it probably should be named Spidle.
\end{description}

% }}}
%-----------------------------------------------------------

\section{Architecture}

\subsection{Modules of Streem}

%-----------------------------------------------------------
% modules to write {{{

\begin{itemize}[noitemsep]
  \item source
    \begin{itemize}[noitemsep]
      \item \sout{persistent} replay (database)
      \item \sout{realtime} continuous (listen for data from network)
      \item pull (subscribe)
      \item feedme
    \end{itemize}
  \item sink
    \begin{itemize}[noitemsep]
      \item push
        \begin{itemize}[noitemsep]
          \item to external daemon (e.g. Fluentd)
          \item \term{feed} stream
        \end{itemize}
      \item publish (for other streams and for clients)
      \item persist (database, logfile, \ldots)
      \item drop
    \end{itemize}
  \item operation
    \begin{itemize}[noitemsep]
      \item stateless
      \item windowed
      \item timed (e.g. MITWA indices)
      \item finite-state machine?
        \begin{itemize}[noitemsep]
          \item full definition could be dificult to embed in Spidle query
            syntax
          \item FSM could be defined externally to the query and then referred
            to by name
          \item in-line FSM definition could be heavily simplified compared to
            full syntax
          \item FSM could be processed externally to whole Streem
        \end{itemize}
    \end{itemize}
  \item stream supervisor (creates and destroys streams, names them, \ldots)
  \item Spidle compiler
\end{itemize}

% }}}
%-----------------------------------------------------------

\subsection{Unstructured notes}

%-----------------------------------------------------------
% properties of things {{{

\begin{itemize}[noitemsep]
  \item stream always has autogenerated name, feedme input and publish output
  \item stream's name may have aliases (well-known names)
  \item stream subscription may specify rejected messages
  \item subscribing to stream's rejected messages include messages rejected in
    somewhere in the middle of stream (what about \verb|+| and rejected?)
  \item \term{select} operation always rejects input messages and always
    produces ($\geq 0$) messages
  \item sink operations sometimes reject input messages and never produce them
  \item streams \term{:all} and \term{:unconsumed}
\end{itemize}

% }}}
%-----------------------------------------------------------
% what to ignore for a prototype {{{

Now some things for me to remember when writing a \emph{prototype}:

\begin{itemize}[noitemsep]
  \item Don't care about loops in the flow.
  \item Don't care about memory usage.
  \item Don't care about processing speed/throughput.
  \item Metadata to messages to be added later.
  \item Window and timed operations to be added later.
  \item FSM is a nice thing to have, but I only have rough idea how should it
    work. Leave for future, or even for an external auxiliary application.
\end{itemize}

% }}}
%-----------------------------------------------------------

\subsection{Main building blocks}

%-----------------------------------------------------------
% operations {{{

Main building blocks are similar to Aurora's (,,Aurora: a new model and
architecture for data stream management'', VLDB Journal (2003)), especially in
how message aggregation works.

Each of the blocks has one message input and two outputs: consumed/processed
messages and rejected messages.

\begin{itemize}
  \item \term{select} rejects all its input unconditionally and produces
    messages out of database, network stream or similar.
  \item \term{grep} accepts some messages according to specified criteria.
  \item \term{map} transforms messages (stateless transformation). Given that
    the message needs to be of appropriate form, \term{map} only accepts some
    messages.
  \item \term{union} produces single stream from several separate streams.
    Marked as a black point in diagrams. It's called \term{concatenate} for
    real-time stream + database and \term{merge} for two databases.
  \item \term{drop} rejects all its input.
  \item \term{aggregate} combines several messages and produces some out of
    them messages on its output. Here it's a catch-all for windowed sum, mean,
    min, max and for windowed sort. Compare with Aurora.
  \item \term{FSM} accepts or rejects various messages, depending on its
    state. A message can cause FSM to change its state, but doesn't need to.
    FSM may reset on timeout. FSM may produce a message on state transition,
    on some other message (without state change) or on timeout.
\end{itemize}

% }}}
%-----------------------------------------------------------

\section{Spidle}

\section{Example queries}

%-----------------------------------------------------------
% simple query {{{

\subsection*{simple query}

\noindent
\begin{minipage}{\textwidth}
\begin{lstlisting}
select foo | grep | map | aggregate
\end{lstlisting}

\begin{tikzpicture}
  [split/.style={circle,minimum size=3pt,fill,inner sep=0pt},
    d/.style={inner sep=0pt},
    op/.style={rectangle,draw,inner sep=4pt,text height=10pt,text depth=4pt}]

  \node[op] (select)             {select};
  \node[op] (grep)      [right=of select] {grep};
  \node[op] (map)       [right=of grep] {map};
  \node[op] (aggregate) [right=of map]    {aggregate};

  \draw [->] (select) -> (grep);
  \draw [->] (grep) -> (map);
  \draw [->] (map) -> (aggregate);
\end{tikzpicture}
\end{minipage}

% }}}
%-----------------------------------------------------------
% tee | (grep1, grep2) {{{

\subsection*{tee $\rightarrow$ grep1, $\rightarrow$ grep2}

\noindent
\begin{minipage}{\textwidth}
\begin{lstlisting}
select | (grep ... | map) + (grep ...) | aggregate
\end{lstlisting}

\begin{tikzpicture}
  [split/.style={circle,minimum size=3pt,fill,inner sep=0pt},
    d/.style={inner sep=0pt},
    op/.style={rectangle,draw,inner sep=4pt,text height=10pt,text depth=4pt}]

  \node[op]    (select)                             {select};
  \node[split] (split)      [right=of select]       {};
  \coordinate  [above=of split] (split top);
  \coordinate  [below=of split] (split down);
  \node[op]    (grep 1)     [right=of split top]    {grep};
  \node[op]    (map 1)      [right=of grep 1]       {map};
  \node[op]    (grep 2)     [right=of split down]   {grep};
  \coordinate  [right=of map 1] (jc map 1);
  \coordinate  (jc) at ($ (select.west)!(jc map 1)!(select.east) $);
  \coordinate  (join top)  at ($ (grep 1.west)!(jc)!(grep 1.east) $);
  \coordinate  (join down) at ($ (grep 2.west)!(jc)!(grep 2.east) $);
  \node[split] (join)       at (jc)                 {};
  \node[op]    (aggregate)  [right=of join]         {aggregate};

  \draw [->] (select) -- (split);
  \draw [->] (split) -- (split top) -- (grep 1);
  \draw [->] (grep 1) -- (map 1);
  \draw [->] (map 1) -- (join top) -- (join);
  \draw [->] (split) -- (split down) -- (grep 2);
  \draw [->] (grep 2) -- (join down) -- (join);
  \draw [->] (join) -- (aggregate);
\end{tikzpicture}
\end{minipage}

% }}}
%-----------------------------------------------------------
% case ... of ... else drop {{{

\subsection*{case ... of ... else drop}

\noindent
\begin{minipage}{\textwidth}
\begin{lstlisting}
select | (grep 1 | map), (grep 2 | map), drop | aggregate
\end{lstlisting}

\begin{tikzpicture}
  [split/.style={circle,minimum size=3pt,fill,inner sep=0pt},
    op/.style={rectangle,draw,inner sep=4pt,text height=10pt,text depth=4pt}]

  \node[op] (select)                                {select};
  \node[op] (grep 1)        [right=of select]       {grep 1?};
  \node[op] (map 1)         [right=of grep 1]       {map 1};
  \node[op] (grep 2)        [below=of grep 1]       {grep 2?};
  \node[op] (map 2)         [right=of grep 2]       {map 2};
  \node[op] (drop)          [below=of grep 2]       {drop};
  \node[split] (join)       [right=of map 1]        {};
  \coordinate  (join down)  at ($ (map 2.west)!(join)!(map 2.east) $) {};
  \node[op] (aggregate)     [right=of join]         {aggregate};

  \draw [->] (select) -- (grep 1);
  \draw [->] (grep 1) -- node [anchor=west]  {no} (grep 2);
  \draw [->] (grep 1) -- node [anchor=south] {yes} (map 1);
  \draw [->] (grep 2) -- node [anchor=west]  {no} (drop);
  \draw [->] (grep 2) -- node [anchor=south] {yes} (map 2);
  \draw [->] (map 1) -- (join);
  \draw [->] (map 2) -- (join down) -- (join);
  \draw [->] (join) -- (aggregate);
\end{tikzpicture}
\end{minipage}

% }}}
%-----------------------------------------------------------
% auxiliary select in the middle of query {{{

\subsection*{select in the middle of query}

\noindent
\begin{minipage}{\textwidth}
\begin{lstlisting}
select X | (grep Q),  (select W) | map ...
select X | (grep Q) + (select W) | map ...
\end{lstlisting}

\begin{tikzpicture}
  [split/.style={circle,minimum size=3pt,fill,inner sep=0pt},
    op/.style={rectangle,draw,inner sep=4pt,text height=10pt,text depth=4pt}]

  \node[op] (select X)                              {select X};
  \node[op] (grep Q)        [right=of select X]     {grep Q?};
  \node[op] (select W)      [below=of grep Q]       {select W};
  \node[split] (join)       [right=of grep Q]       {};
  \coordinate  (join down)  at ($ (select W.west)!(join)!(select W.east) $);
  \node[op] (aggregate)     [right=of join]         {aggregate};

  \draw [->] (select X) -- (grep Q);
  \draw [->] (grep Q) -- node [anchor=south] {yes} (join);
  \draw [->,dotted] (grep Q) -- node [anchor=west] {no} (select W);
  \draw [->] (select W) -- (join down) -- (join);
  \draw [->] (join) -- (aggregate);
\end{tikzpicture}
\end{minipage}

% }}}
%-----------------------------------------------------------

\subsection{\term{Map} syntax}

%-----------------------------------------------------------
% map syntax {{{

Based on Erlang's pattern matching and a little on Perl and Python. Not
a finished thing yet, subject to heavy changes.

\begin{minipage}{\textwidth}
\begin{lstlisting}
{"foo": [Bar, *Baz] = Foo,
  "host": H ~ /^es/i,
  "service": ?Svc,
  "level": L > 10,
  *Rest}, Bar == len(Bar) ->
{"hostname": H, "doc": {*Rest}, "svc": Svc,
   "qq": Rest["qq"], "foo": {"foo[0]": Bar, "foo[1:]": [*Baz]}
   "rest keys": [Rest.keys], "rest values": [Rest.values],
   "rest (k,v) pairs": [Rest.pairs], "Rest[foo[0]]": Rest[Bar] }
\end{lstlisting}
\end{minipage}

Variables defined in \verb+(...)+ scope are accessible in this scope in later
processing rules. They can't be used for retrospective flow control. They
probably can be used by \term{group by} (SQL-like) operator in aggregation, if
the variable is carried by the message as its metadata.

% }}}
%-----------------------------------------------------------

\end{document}

%-----------------------------------------------------------------------------
% vim:ft=tex:foldmethod=marker
