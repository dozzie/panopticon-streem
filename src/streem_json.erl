%%%---------------------------------------------------------------------------
%%% @doc
%%%   JSON parser and serializer.
%%%
%%%   Justification of having a separate parser:
%%%   <ul>
%%%     <li>reduction of external dependencies</li>
%%%     <li>speed (decoding nearly two times faster than jsx)</li>
%%%     <li>API better than jsx (errors reported by returned value instead of
%%%         throwing an excpetion)</li>
%%%   </ul>
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_json).

-export([decode/1, load/1]).
-export([encode/1, dump/1]).

%%%---------------------------------------------------------------------------

%% @type jsx_value() = jsx_scalar() | jsx_array() | jsx_hash().

%% @type jsx_scalar() = null | true | false | jsx_number() | jsx_string().
%%   On data serialization, `undefined' is treated as `null'.

%% @type jsx_string() = binary().
%%   On data serialization, atoms (other than `null', `undefined', `true' or
%%   `false') are treated as strings.

%% @type jsx_number() = integer() | float().

%% @type jsx_array() = [jsx_value()].

%% @type jsx_hash() = [{}] | [{jsx_string(), jsx_value()}].
%%   On serialization, {@type string()} is allowed instead of
%%   {@type jsx_string()}.

%%%---------------------------------------------------------------------------
%%% decode JSON to jsx structure
%%%---------------------------------------------------------------------------

%% @doc Load JSON structure from string.
%%
%% @spec load(string() | binary()) ->
%%   {ok, jsx_value()} | {error, Reason}

load(String) ->
  decode(String).

%% @doc Load JSON structure from string.
%%
%% @spec decode(string() | binary()) ->
%%   {ok, jsx_value()} | {error, Reason}

decode(String) when is_binary(String) ->
  case unicode:characters_to_list(String) of
    List when is_list(List) -> decode(List);
    {error, _, _} -> {error, bad_unicode};
    {incomplete, _, _} -> {error, incomplete_unicode}
  end;

decode(String) when is_list(String) ->
  try
    {Data, Tail} = decode_value(String),
    "" = skip_spaces(Tail),
    {ok, Data}
  catch
    throw:{error, Reason} ->
      {error, Reason};
    % NOTE: `{case_clause,V}', `if_clause' and `{try_clause,V}' are not used
    % here
    error:{badmatch, _Value} ->
      {error, badarg};
    error:function_clause ->
      {error, badarg}
  end.

%%%---------------------------------------------------------------------------
%%% encode jsx structure to JSON
%%%---------------------------------------------------------------------------

%% @doc Serialize jsx data structure.
%%
%% @spec dump(jsx_value()) ->
%%   {ok, iolist()} | {error, Reason}

dump(Data) ->
  encode(Data).

%% @doc Serialize jsx data structure.
%%
%% @spec encode(jsx_value()) ->
%%   {ok, iolist()} | {error, Reason}

encode(Data) ->
  try
    Result = encode_value(Data),
    {ok, Result}
  catch
    throw:{error, Reason} ->
      {error, Reason};
    % NOTE: `{case_clause,V}', `if_clause' and `{try_clause,V}' are not used
    % here
    error:{badmatch, _Value} ->
      {error, badarg};
    error:function_clause ->
      {error, badarg}
  end.

%%%---------------------------------------------------------------------------
%%% decode helpers
%%%---------------------------------------------------------------------------

%%----------------------------------------------------------
%% decode_value(JSON) {{{

decode_value("[" ++ JSON) ->
  {Array, Rest} = decode_array(skip_spaces(JSON)),
  "]" ++ RestAfterArray = Rest,
  {Array, skip_spaces(RestAfterArray)};

decode_value("{" ++ JSON) ->
  {Object, Rest} = decode_object(skip_spaces(JSON)),
  "}" ++ RestAfterObject = Rest,
  {Object, skip_spaces(RestAfterObject)};

decode_value(JSON) ->
  decode_scalar(skip_spaces(JSON)).

%% }}}
%%----------------------------------------------------------
%% decode_array(JSON) {{{

decode_array("]" ++ _ = JSON) ->
  {[], JSON}; % short-circuit for empty arrays

decode_array(JSON) ->
  decode_list(JSON, fun decode_value/1).

%% }}}
%%----------------------------------------------------------
%% decode_object(JSON) {{{

decode_object("}" ++ _ = JSON) ->
  {[{}], JSON}; % short-circuit for empty hashes

decode_object(JSON) ->
  decode_list(JSON, fun decode_key_value/1).

decode_key_value("\"" ++ JSON) ->
  {Key, ":" ++ ValueString} = decode_string(JSON, ""),
  {Value, Rest} = decode_value(skip_spaces(ValueString)),
  {{Key, Value}, Rest}.

%% }}}
%%----------------------------------------------------------
%% decode_list(JSON, DecodeElement) {{{

decode_list(JSON, DecodeElement) ->
  {FirstElement, Rest} = DecodeElement(JSON),
  decode_list_tail(Rest, DecodeElement, [FirstElement]).

decode_list_tail("," ++ JSON, DecodeElement, Acc) ->
  {Value, Rest} = DecodeElement(skip_spaces(JSON)),
  decode_list_tail(Rest, DecodeElement, [Value | Acc]);

decode_list_tail(JSON, _DecodeElement, Acc) ->
  {lists:reverse(Acc), skip_spaces(JSON)}.

%% }}}
%%----------------------------------------------------------
%% decode_scalar(JSON) {{{

decode_scalar("-" ++ JSON) ->
  {Number, Rest} = decode_number(JSON), % NOTE: no skip_spaces()
  {-Number, Rest};

decode_scalar([C | _] = JSON) when C >= $0, C =< $9 ->
  decode_number(JSON);

decode_scalar("\"" ++ JSON) ->
  decode_string(JSON, "");

decode_scalar("null" ++ JSON) ->
  {null, skip_spaces(JSON)};

decode_scalar("true" ++ JSON) ->
  {true, skip_spaces(JSON)};

decode_scalar("false" ++ JSON) ->
  {false, skip_spaces(JSON)}.

%% }}}
%%----------------------------------------------------------
%% decode_string(JSON, Init) {{{

decode_string("\"" ++ JSON, Acc) ->
  String = lists:reverse(Acc),
  case unicode:characters_to_binary(String) of
    Binary when is_binary(Binary) ->
      {Binary, skip_spaces(JSON)};
    {error, _, _} ->
      throw({error, bad_unicode});
    {incomplete, _, _} ->
      throw({error, incomplete_unicode})
  end;
decode_string("\\\"" ++ JSON, Acc) -> decode_string(JSON, "\"" ++ Acc);
decode_string("\\\\" ++ JSON, Acc) -> decode_string(JSON, "\\" ++ Acc);
decode_string("\\/" ++ JSON, Acc)  -> decode_string(JSON, "/"  ++ Acc);
decode_string("\\b" ++ JSON, Acc)  -> decode_string(JSON, "\b" ++ Acc);
decode_string("\\f" ++ JSON, Acc)  -> decode_string(JSON, "\f" ++ Acc);
decode_string("\\n" ++ JSON, Acc)  -> decode_string(JSON, "\n" ++ Acc);
decode_string("\\r" ++ JSON, Acc)  -> decode_string(JSON, "\r" ++ Acc);
decode_string("\\t" ++ JSON, Acc)  -> decode_string(JSON, "\t" ++ Acc);
decode_string("\\u" ++ JSON, Acc) ->
  {Char, Rest} = decode_u(JSON),
  decode_string(Rest, [Char | Acc]);
decode_string([Char | JSON], Acc) -> decode_string(JSON, [Char | Acc]).

hex(C) when C >= $0, C =< $9 -> C - $0;
hex(C) when C >= $a, C =< $f -> C - $a + 10;
hex(C) when C >= $A, C =< $F -> C - $A + 10.

decode_u([C1, C2, C3, C4 | Rest]) ->
  Char = hex(C1) * 16 * 16 * 16 +
         hex(C2) * 16 * 16 +
         hex(C3) * 16 +
         hex(C4),
  {Char, Rest}.

%% }}}
%%----------------------------------------------------------
%% decode_number(JSON) {{{

decode_number(JSON) ->
  % fortunately this (mostly) matches JSON specification
  case string:to_float(JSON) of
    {error, no_float} ->
      % XXX: `JSON' starts with at least one digit
      {Integer, IntegerTail} = string:to_integer(JSON),
      {Integer, skip_spaces(IntegerTail)};
    {Float, FloatTail} when is_float(Float) ->
      {Float, skip_spaces(FloatTail)}
  end.

%% }}}
%%----------------------------------------------------------
%% skip_spaces(JSON) {{{

skip_spaces([C | JSON]) when C == $ ; C == $\n; C == $\t; C == $\r ->
  skip_spaces(JSON);
skip_spaces(JSON) ->
  JSON.

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% encode helpers
%%%---------------------------------------------------------------------------

%%----------------------------------------------------------
%% encode_value(Value) {{{

encode_value([{}] = _Value) ->
  "{}";
encode_value([{_K,_V} = _Element | _Rest] = Value) ->
  [${, encode_list(Value, fun encode_key_value/1), $}];

encode_value([] = _Value) ->
  "[]";
encode_value([_Element | _Rest] = Value) ->
  [$[, encode_list(Value, fun encode_value/1), $]];

encode_value(Value) ->
  encode_scalar(Value).

%% }}}
%%----------------------------------------------------------
%% encode_key_value(KVList) {{{

encode_key_value({Key, Value} = _Element) ->
  [encode_string(Key), ":", encode_value(Value)].

%% }}}
%%----------------------------------------------------------
%% encode_scalar(Value) {{{

encode_scalar(Value) when is_float(Value) ->
  float_to_list(Value); % TODO: minimize the output (e.g. excessive zeros)
encode_scalar(Value) when is_integer(Value) ->
  integer_to_list(Value);

encode_scalar(true)  -> "true";
encode_scalar(false) -> "false";
encode_scalar(null)  -> "null";
encode_scalar(undefined) -> "undefined";

encode_scalar(Value) ->
  encode_string(Value).

%% }}}
%%----------------------------------------------------------
%% encode_string(Value) {{{

encode_string(Value) when is_list(Value) ->
  [$", escape(Value), $"];

encode_string(Value) when is_binary(Value) ->
  case unicode:characters_to_list(Value) of
    List when is_list(List) -> encode_string(List);
    {error, _, _} -> throw({error, bad_unicode});
    {incomplete, _, _} -> throw({error, incomplete_unicode})
  end;

encode_string(Value) when is_atom(Value) ->
  encode_string(atom_to_list(Value)).

escape("") -> "";
escape("\"" ++ Rest) -> "\\\"" ++ escape(Rest);
escape("\\" ++ Rest) -> "\\\\" ++ escape(Rest);
escape("\b" ++ Rest) -> "\\b"  ++ escape(Rest);
escape("\f" ++ Rest) -> "\\f"  ++ escape(Rest);
escape("\n" ++ Rest) -> "\\n"  ++ escape(Rest);
escape("\r" ++ Rest) -> "\\r"  ++ escape(Rest);
escape("\t" ++ Rest) -> "\\t"  ++ escape(Rest);
escape([C | Rest]) when C >= 32, C =< $~ -> [C | escape(Rest)];
escape([C | Rest]) ->
  CharRepr = case erlang:integer_to_list(C, 16) of
    [_]          = U -> "\\u000" ++ U;
    [_, _]       = U -> "\\u00"  ++ U;
    [_, _, _]    = U -> "\\u0"   ++ U;
    [_, _, _, _] = U -> "\\u"    ++ U;
    _ -> throw({error, bad_unicode})
  end,
  [CharRepr | escape(Rest)].

%% }}}
%%----------------------------------------------------------
%% encode_list(Elements, EncodeElement) {{{

encode_list([Element] = _Elements, EncodeElement) ->
  [EncodeElement(Element)];

encode_list([Element | Rest] = _Elements, EncodeElement) ->
  [EncodeElement(Element), "," | encode_list(Rest, EncodeElement)].

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
