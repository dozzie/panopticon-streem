%%%---------------------------------------------------------------------------
%%% @private
%%% @doc
%%%   <i>Map</i> operation.
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_op_map).

-behaviour(gen_streem_op).

%% gen_streem_op API
-export([init/1, terminate/1, process_message/2]).

-record(state, {
  op,                 % set | unset | rename
  key, value,         % for set | unset
  old_name, new_name  % for rename
}).

%%%---------------------------------------------------------------------------
%%% types

%% @type operation() =
%%     {set, Key :: binary(), {value, Value :: term()}}
%%   | {set, Key :: binary(), {field, Name  :: binary()}}
%%   | {unset, Key :: binary()}
%%   | {rename, OldName :: binary(), NewName :: binary()}.

-type operation() ::
    {set, Key :: binary(), {value, Value :: term()}}
  | {set, Key :: binary(), {field, Name  :: binary()}}
  | {unset, Key :: binary()}
  | {rename, OldName :: binary(), NewName :: binary()}.

%% @type state() = #state{}.

-type state() :: #state{}.

%%%---------------------------------------------------------------------------
%%% gen_streem_op API
%%%---------------------------------------------------------------------------

%% @doc Initialize the block.
%%
%% @spec init(any()) ->
%%  {ok, state()} | {error, term()}

-spec init(operation()) ->
  {ok, state()} | {error, term()}.

init({set, Key, {value, Value}} = _Operation) when is_binary(Key) ->
  {ok, #state{op = set, key = Key, value = {value, Value}}};

init({set, Key, {field, Name}} = _Operation) when is_binary(Key) ->
  {ok, #state{op = set, key = Key, value = {field, Name}}};

init({unset, Key} = _Operation) when is_binary(Key) ->
  {ok, #state{op = unset, key = Key}};

init({rename, OldName, NewName} = _Operation)
when is_binary(OldName) andalso is_binary(NewName) ->
  {ok, #state{op = rename, old_name = OldName, new_name = NewName}};

init(_Operation) ->
  {error, badarg}.

%% @doc Clean up the block on shutdown.
%%
%% @spec terminate(state()) ->
%%  any()

-spec terminate(state()) ->
  any().

terminate(_State) ->
  ok.

%% @doc Handle single message.
%%
%% @spec process_message(streem_pipeline:message(), state()) ->
%%    {accept, streem_pipeline:message(), state()}

-spec process_message(streem_pipeline:message(), state()) ->
  {accept, streem_pipeline:message(), state()}.

process_message({Payload, Metadata} = _Message, State) ->
  {accept, {alter(Payload, State), Metadata}, State}.

%%%---------------------------------------------------------------------------

%% @doc Alter the message according to the operation definition.

-spec alter(streem_pipeline:payload(), state()) ->
  streem_pipeline:payload().

alter(Message, _State = #state{op = set, key = Key, value = {value, Value}}) ->
  [{Key, Value} | proplists:delete(Key, Message)];

alter(Message, _State = #state{op = set, key = Key, value = {field, Field}}) ->
  case proplists:lookup(Field, Message) of
    {Field, Value} ->
      [{Key, Value} | proplists:delete(Key, Message)];
    none ->
      Message
  end;

alter(Message, _State = #state{op = unset, key = Key}) ->
  proplists:delete(Key, Message);

alter(Message, _State = #state{op = rename, old_name = Old, new_name = New}) ->
  case proplists:lookup(Old, Message) of
    {Old, Value} ->
      [{New, Value} | proplists:delete(Old, Message)];
    none ->
      Message
  end;

alter(Message, _State) ->
  Message. % otherwise no-op

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
