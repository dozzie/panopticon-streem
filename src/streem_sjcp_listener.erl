%%%---------------------------------------------------------------------------
%%% @private
%%% @doc
%%%   SJCP socket listener.
%%%
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_sjcp_listener).

-behaviour(gen_server).

%% public API
-export([]).

%% starting process
-export([start_link/2]).

%% gen_server callbacks
-export([init/1, terminate/2]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-export([code_change/3]).

-define(ACCEPT_TIMEOUT, 300). % chosen arbitrarily
-record(state, {sock, workers_sup}).

%%%---------------------------------------------------------------------------
%%% public API
%%%---------------------------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% starting process
%%%---------------------------------------------------------------------------

%% @doc Start listener process.
%%
%% @spec start_link(pid(), {any | string() | inet:ip_address(), integer()}) ->
%%   {ok, pid()} | {error, Reason}

-spec start_link(pid(), {any | string() | inet:ip_address(), integer()}) ->
  {ok, pid()} | {error, term()}.

start_link(Supervisor, AddrSpec) ->
  gen_server:start_link(?MODULE, [Supervisor, AddrSpec], []).

%%%---------------------------------------------------------------------------
%%% gen_server callbacks
%%%---------------------------------------------------------------------------

%%----------------------------------------------------------
%% init/cleanup {{{

%% @private
%% @doc Process initialization.

init([Supervisor, {ListenAddr, Port} = _AddrSpec]) ->
  SocketOptions = [
    {active, false}, binary, {packet, line},
    {reuseaddr, true}, {keepalive, true}
  ],
  {ok, Sock} = case ListenAddr of
    any ->
      gen_tcp:listen(Port, SocketOptions);
    {_,_,_,_} ->
      gen_tcp:listen(Port, [{ip, ListenAddr} | SocketOptions])
  end,

  % I can't call this supervisor, since it is already busy calling this
  % function
  self() ! {prepare_clients, Supervisor},

  % no timeout yet, just wait for the `{prepare_clients,pid()}' message
  {ok, #state{sock = Sock}}.

%% @private
%% @doc Process cleanup.

terminate(_Reason, _State = #state{sock = Sock}) ->
  gen_tcp:close(Sock),
  ok.

%% }}}
%%----------------------------------------------------------
%% communication with process {{{

%% @private
%% @doc Handle {@link gen_server:call/2}.

%% unknown call -- ignore
handle_call(_Request, _From, State) ->
  {reply, ok, State, 0}.

%% @private
%% @doc Handle {@link gen_server:cast/2}.

%% unknown cast -- ignore
handle_cast(_Request, State) ->
  {noreply, State, 0}. % ignore unknown calls

%% @private
%% @doc Handle incoming messages.

%% just return to waiting for clients
handle_info(timeout, State = #state{sock = Sock, workers_sup = WorkSup}) ->
  case gen_tcp:accept(Sock, ?ACCEPT_TIMEOUT) of
    {ok, Client} ->
      {ok, Pid} = streem_sjcp_sup:spawn_worker(WorkSup, Client),
      % FIXME: race condition (worker set socket to active, client sent
      % a line, listener hasn't changed controlling process)
      gen_tcp:controlling_process(Client, Pid),
      ok = inet:setopts(Client, [{active, true}]),
      ok;
    {error, timeout} ->
      ok;
    {error, _Any} ->
      % TODO: log this? die?
      ignore
  end,
  {noreply, State, 0};

handle_info({prepare_clients, Supervisor}, State) ->
  {ok, Pid} = streem_sjcp_sup:workers_supervisor(Supervisor),
  {noreply, State#state{workers_sup = Pid}, 0};

%% unknown message -- ignore
handle_info(_Message, State) ->
  {noreply, State, 0}.

%% }}}
%%----------------------------------------------------------
%% code change {{{

%% @private
%% @doc Handle code change notification.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
