%%%---------------------------------------------------------------------------
%%% @private
%%% @doc
%%%   SJCP top supervisor.
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_sjcp_sup).

-behaviour(supervisor).

%% public API
-export([workers_supervisor/1, spawn_worker/2]).

%% starting process
-export([start_link/1]).

%% supervisor callbacks
-export([init/1]).

%%%---------------------------------------------------------------------------
%%% public API
%%%---------------------------------------------------------------------------

%% @doc Prepare supervisor for SJCP workers (client handlers).
%%
%% @spec workers_supervisor(pid()) ->
%%   pid()

-spec workers_supervisor(pid()) ->
  {ok, pid()}.

workers_supervisor(SupSupervisor) ->
  Children = supervisor:which_children(SupSupervisor),
  {_Id, Pid, supervisor, _} = lists:keyfind(workers_sup, 1, Children),
  {ok, Pid}.

%% @doc Spawn a supervised worker.
%%
%% @spec spawn_worker(pid(), term()) ->
%%   {ok, pid()} | {error, Reason}

spawn_worker(WorkerSup, Socket) ->
  case supervisor:start_child(WorkerSup, [Socket]) of
    {ok, Pid}        -> {ok, Pid};
    {ok, Pid, _Info} -> {ok, Pid};
    Any -> Any
  end.

%%%---------------------------------------------------------------------------
%%% starting process
%%%---------------------------------------------------------------------------

%% @doc Start supervisor process.
%%
%% @spec start_link({BindAddr :: any | string() | inet:ip_address(),
%%                    Port :: integer()}) ->
%%   {ok, pid()} | {error, Reason}

-spec start_link({any | string() | inet:ip_address(), integer()}) ->
  {ok, pid()} | {error, term()}.

start_link(AddrSpec) ->
  supervisor:start_link(?MODULE, AddrSpec).

%%%---------------------------------------------------------------------------
%%% supervisor callbacks
%%%---------------------------------------------------------------------------

%% @doc Return tree specification.

init(AddrSpec) ->
  Strategy = {one_for_all, 5, 10},
  Children = [
    {workers_sup,
      {streem_sjcp_client_sup, start_link, []},
      permanent, infinity, supervisor, [streem_sjcp_client_sup]},
    {listener,
      {streem_sjcp_listener, start_link, [self(), AddrSpec]},
      permanent, 1000, worker, [streem_sjcp_listener]}
  ],
  {ok, {Strategy, Children}}.

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
