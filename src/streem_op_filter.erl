%%%---------------------------------------------------------------------------
%%% @private
%%% @doc
%%%   <i>Filter</i> operation.
%%%
%%% @TODO Filter not only against hardcoded (provided) value, but also against
%%%   value of other field.
%%%
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_op_filter).

-behaviour(gen_streem_op).

%% gen_streem_op API
-export([init/1, terminate/1, process_message/2]).

-record(state, {
  cmp,    % '=' | '!=' | '<=' | '<' | '>' | '>=' | regexp | exists
  field,  % binary()
  value,  % integer() | float() | binary() | true | false | null
  negated = false  % false | true
}).

%%%---------------------------------------------------------------------------
%%% types

%% @type state() = #state{}.

-type state() :: #state{}.

%%%---------------------------------------------------------------------------
%%% gen_streem_op API
%%%---------------------------------------------------------------------------

%% @doc Initialize the block.
%%
%% @spec init(any()) ->
%%  {ok, state()} | {error, Reason}

-spec init(any()) ->
  {ok, state()} | {error, term()}.

init({Name, Cmp, Value} = _Operation)
when Cmp == '=' orelse Cmp == '!=' orelse
     Cmp == '<' orelse Cmp == '<=' orelse Cmp == '>' orelse Cmp == '>=' ->
  {ok, #state{cmp = Cmp, field = Name, value = Value}};

init({Name, regexp, Regexp} = _Operation) ->
  case re:compile(Regexp, []) of
    {ok, RegexpCompiled} ->
      {ok, #state{cmp = regexp, field = Name, value = RegexpCompiled}};
    {error, _Reason} ->
      {error, bad_regexp}
  end;

init({Name, exists} = _Operation) ->
  {ok, #state{cmp = exists, field = Name}};

init(_Operation) ->
  {error, badarg}.

%% @doc Clean up the block on shutdown.
%%
%% @spec terminate(state()) ->
%%  any()

-spec terminate(state()) ->
  any().

terminate(_State) ->
  ok.

%% @doc Handle single message.
%%
%% @spec process_message(streem_pipeline:message(), state()) ->
%%    {accept, streem_pipeline:message(), state()}
%%  | reject
%%  | {consume, state()}

-spec process_message(streem_pipeline:message(), state()) ->
    {accept, streem_pipeline:message(), state()}
  | reject.

process_message({Payload, _Metadata} = Message, State) ->
  case accept(Payload, State) of
    true  -> {accept, Message, State};
    false -> reject
  end.

%%%---------------------------------------------------------------------------

%% @doc Check message against the filter.

accept(Message, _State = #state{cmp = exists, field = Name}) ->
  case proplists:lookup(Name, Message) of
    {_, _} -> true;
    none   -> false
  end;

accept(Message, _State = #state{cmp = regexp, field = Name, value = Re}) ->
  case proplists:lookup(Name, Message) of
    {Name, Value} when is_binary(Value) ->
      % only true if matches
      match == re:run(Value, Re, [{capture, none}]);
    _Any ->
      % either not a string or no field at all
      false
  end;

accept(Message, _State = #state{cmp = Cmp, field = Name, value = Reference}) ->
  case proplists:lookup(Name, Message) of
    {Name, Value} ->
      case {typeof(Value), Cmp, typeof(Reference)} of
        {null, '=', null} ->
          true;
        {Type, '!=', null} when Type /= null ->
          true;
        {Type,_,Type}
        when Type == bool orelse Type == number orelse Type == string ->
          % comparable types
          cmp(Value, Cmp, Reference);
        _Any ->
          % types don't match
          false
      end;
    none ->
      % comparing to non-existing key always fails
      false
  end.

%% @doc Compare two values.
%%   On `` '=' '' and `` '!=' '' comparison operators are, accordingly, `=='
%%   and `/='.
%%
%% @spec cmp(term(), '=' | '!=' | '<=' | '<' | '>' | '>=', term()) ->
%%   true | false

-spec cmp(term(), '=' | '!=' | '<=' | '<' | '>' | '>=', term()) ->
  true | false.

cmp(A,  '=' = _Cmp, B) -> A == B;
cmp(A,  '<' = _Cmp, B) -> A  < B;
cmp(A,  '>' = _Cmp, B) -> A  > B;
cmp(A, '!=' = _Cmp, B) -> A /= B;
cmp(A, '<=' = _Cmp, B) -> A =< B;
cmp(A, '>=' = _Cmp, B) -> A >= B.

%% @doc Detect type of the value.
%%
%% @spec typeof(term()) ->
%%   null | bool | number | string | hash | array | unknown

-spec typeof(term()) ->
  null | bool | number | string | hash | array | unknown.

typeof(null = _A) ->
  null;
typeof(true = _A) ->
  bool;
typeof(false = _A) ->
  bool;
typeof(A) when is_float(A) ->
  number;
typeof(A) when is_integer(A) ->
  number;
typeof(A) when is_binary(A) ->
  string;
typeof([{}] = _A) ->
  hash;
typeof([{_,_} | _] = _A) ->
  hash;
typeof(A) when is_list(A) ->
  array;
typeof(_A) ->
  unknown.

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
