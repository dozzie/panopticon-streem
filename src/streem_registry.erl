%%%---------------------------------------------------------------------------
%%% @doc
%%%   Stream registry process. The process keeps mappings: name to PID
%%%   and PID to name. It also keeps track of subscriptions, so newly
%%%   registered streams already can start passing data.
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_registry).

-behaviour(gen_server).

%% public API
-export([register/1, unregister/1, unregister/0]).
-export([subscribe/1, unsubscribe/1, unsubscribe/0]).
-export([names_of/1, pids_of/1]).

-export_type([stream_name/0, unique_name/0, well_known_name/0]).

%% API for internal use in Streem
-export([register/2, unregister/2]).
-export([subscribe/2, unsubscribe/2]).

%% starting process
-export([start/0, start_link/0]).

%% gen_server callbacks
-export([init/1, terminate/2]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-export([code_change/3]).

-record(state, {}).

%%%---------------------------------------------------------------------------
%%% types

%% @type stream_name() = unique_name() | well_known_name().

-type stream_name() :: unique_name() | well_known_name().

%% @type unique_name() = string().
%%   Name of this type starts with `"U:"'.

-type unique_name() :: string().

%% @type well_known_name() = string().
%%   Name of this type doesn't contain colons.

-type well_known_name() :: string().

%% @type subscribers() = [pid()].
%%   List of subscribers for given stream name.

-type subscribers() :: [pid()].

%%%---------------------------------------------------------------------------
%%% public API
%%%---------------------------------------------------------------------------

%%----------------------------------------------------------
%% finding names for processes and vice versa {{{

%% find_pid(Name :: stream_name()) -> pid().
%% find_names(Pid :: pid()) -> [stream_name()].

%% @doc Find all names of specified stream.
%%
%% @spec names_of(pid()) ->
%%   [stream_name()]

-spec names_of(pid()) ->
  [stream_name()].

names_of(Pid) ->
  gen_server:call(?MODULE, {names_of, Pid}).

%% @doc Find all streams that publish messages under specified name.
%%
%% @spec pids_of(stream_name()) ->
%%   [pid()]

-spec pids_of(stream_name()) ->
  [pid()].

pids_of(Name) ->
  gen_server:call(?MODULE, {pids_of, Name}).

%% }}}
%%----------------------------------------------------------
%% registering names {{{

%% @doc Register the stream under specified names.
%%   Function should be called in process that executes stream pipeline.
%%
%%   Function returns (current) list of processes that subscribe requested
%%   names.
%%
%% @spec register([well_known_name()]) ->
%%   {ok, subscribers()} | {error, Reason}

-spec register([well_known_name()]) ->
  {ok, subscribers()} | {error, term()}.

register(Names) ->
  gen_server:call(?MODULE, {register, self(), Names}).

%% @private
%% @doc Register specified stream under specified names.
%%
%%   Function returns (current) list of processes that subscribe requested
%%   names.
%%
%% @spec register(pid(), [well_known_name()]) ->
%%   {ok, subscribers()} | {error, Reason}

-spec register(pid(), [well_known_name()]) ->
  {ok, subscribers()} | {error, term()}.

register(Pid, Names) ->
  gen_server:call(?MODULE, {register, Pid, Names}).

%% @doc Unregister the stream from specified names.
%%   Function should be called in process that executes stream pipeline.
%%
%%   Function returns (current) list of processes subscribing names that
%%   were not removed.
%%
%% @spec unregister([well_known_name()]) ->
%%   {ok, subscribers()} | {error, Reason}

-spec unregister([well_known_name()]) ->
  {ok, subscribers()} | {error, term()}.

unregister(Names) ->
  gen_server:call(?MODULE, {unregister, self(), Names}).

%% @private
%% @doc Unregister specified stream from specified names.
%%   Function should be called in process that executes stream pipeline.
%%
%%   Function returns (current) list of processes subscribing names that
%%   were not removed.
%%
%% @spec unregister(pid(), [well_known_name()]) ->
%%   {ok, subscribers()} | {error, Reason}

-spec unregister(pid(), [well_known_name()]) ->
  {ok, subscribers()} | {error, term()}.

unregister(Pid, Names) ->
  gen_server:call(?MODULE, {unregister, Pid, Names}).

%% @doc Unregister the stream altogether.
%%
%%   Function's main use is for shutting the pipeline down.
%%
%% @spec unregister() ->
%%   ok

-spec unregister() ->
  ok.

unregister() ->
  gen_server:call(?MODULE, {unregister, self()}).

%% }}}
%%----------------------------------------------------------
%% stream subscription {{{

%% @doc Subscribe to a stream.
%%
%%   It's OK to pass a name that hasn't been registered yet. Once any stream
%%   registers it, the subscriber will start receiving messages from the
%%   stream.
%%
%%   The only exception is {@type unique_name()}: subscriptions to such name
%%   are automatically dropped when {@type unique_name()} is not registered
%%   (either because the the name became unregistered or it wasn't registered
%%   at all). This is because a) {@type unique_name()} can't be predicted in
%%   advance and b) will never show up in the future, so there's no point in
%%   remembering subscriptions to non-existing {@type unique_name()}.
%%
%% @spec subscribe(stream_name()) ->
%%   ok | {error, Reason}

-spec subscribe(stream_name()) ->
  ok | {error, term()}.

subscribe(Name) ->
  gen_server:call(?MODULE, {subscribe, self(), Name}).

%% @private
%% @doc Subscribe process to a stream.
%%
%% @spec subscribe(pid(), stream_name()) ->
%%   ok | {error, Reason}

-spec subscribe(pid(), stream_name()) ->
  ok | {error, term()}.

subscribe(Pid, Name) ->
  gen_server:call(?MODULE, {subscribe, Pid, Name}).

%% @doc Unsubscribe from a stream.
%%
%% @spec unsubscribe(stream_name()) ->
%%   ok

-spec unsubscribe(stream_name()) ->
  ok.

unsubscribe(Name) ->
  gen_server:call(?MODULE, {unsubscribe, self(), Name}).

%% @private
%% @doc Unsubscribe process from a stream.
%%
%% @spec unsubscribe(pid(), stream_name()) ->
%%   ok

-spec unsubscribe(pid(), stream_name()) ->
  ok.

unsubscribe(Pid, Name) ->
  gen_server:call(?MODULE, {unsubscribe, Pid, Name}).

%% @doc Unsubscribe from all streams.
%%
%% @spec unsubscribe() ->
%%   ok

-spec unsubscribe() ->
  ok.

unsubscribe() ->
  gen_server:call(?MODULE, {unsubscribe, self()}).

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% starting process
%%%---------------------------------------------------------------------------

%% @doc Start a registry process.
%%
%%   Function registers the process locally, so only one can be started in
%%   Erlang VM.
%%
%% @spec start() ->
%%   {ok, pid()} | {error, Reason}

-spec start() ->
  {ok, pid()} | {error, term()}.

start() ->
  gen_server:start({local, ?MODULE}, ?MODULE, none, []).

%% @doc Start (and link) a registry process.
%%
%%   Function registers the process locally, so only one can be started in
%%   Erlang VM.
%%
%% @spec start_link() ->
%%   {ok, pid()} | {error, Reason}

-spec start_link() ->
  {ok, pid()} | {error, term()}.

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).

%%%---------------------------------------------------------------------------
%%% gen_server callbacks
%%%---------------------------------------------------------------------------

%% @private
%% @doc Registry process initialization.

init(_Args) ->
  % bag can have duplicated objects for given key
  % keypos differs so I can keep the same object in both complimentary tables
  % XXX: name is always the first element in the tuple
  ets:new(name_to_pid, [named_table, bag, {keypos,1}]),
  ets:new(pid_to_name, [named_table, bag, {keypos,2}]),
  ets:new(name_to_subscriber, [named_table, bag, {keypos,1}]),
  ets:new(subscriber_to_name, [named_table, bag, {keypos,2}]),
  % for monitoring:
  %   * `{{subscriber, pid()}, reference()}'
  %   * `{{stream,     pid()}, reference()}'
  % a prorcess (stream) may be monitored in both modes
  ets:new(monitor, [named_table]),
  {ok, #state{}}.

%% @private
%% @doc Process cleanup.

terminate(_Reason, _State) ->
  % the process will probably terminate soon, but it's good manners to free
  % ETS tables
  ets:delete(name_to_pid),
  ets:delete(pid_to_name),
  ets:delete(name_to_subscriber),
  ets:delete(subscriber_to_name),
  ok.

%% @private
%% @doc Handle {@link gen_server:call/2}.

handle_call({debug, dump_ets}, _From, State) ->
  Pid2Name = ets:foldl(fun(A,Acc) -> [A | Acc] end, [], pid_to_name),
  Name2Pid = ets:foldl(fun(A,Acc) -> [A | Acc] end, [], name_to_pid),
  Sub2Name = ets:foldl(fun(A,Acc) -> [A | Acc] end, [], subscriber_to_name),
  Name2Sub = ets:foldl(fun(A,Acc) -> [A | Acc] end, [], name_to_subscriber),
  Result = {
    {streams,     lists:keysort(1, Pid2Name), lists:keysort(1, Name2Pid)},
    {subscribers, lists:keysort(1, Sub2Name), lists:keysort(1, Name2Sub)}
  },
  {reply, Result, State};

%%----------------------------------------------------------
%% stream registration {{{

handle_call({register, PipePid, Names} = _Request, _From, State) ->
  case correct_well_known_names(Names) of
    true ->
      % if the stream doesn't have a unique name yet, add one
      case ets:member(pid_to_name, PipePid) of
        true  -> ok;
        false ->
          UniqueName = generate_name(),
          ets:insert(pid_to_name, {UniqueName, PipePid}),
          ets:insert(name_to_pid, {UniqueName, PipePid}),
          ok
      end,
      setup_monitor(stream, PipePid),
      ToInsert = [{Name, PipePid} || Name <- Names],
      ets:insert(name_to_pid, ToInsert),
      ets:insert(pid_to_name, ToInsert),
      % return subscribers for all names, including previously registered ones
      {reply, {ok, subscribers(PipePid)}, State};
    false ->
      {reply, {error, badarg}, State}
  end;

handle_call({unregister, _PipePid, "U:" ++ _Name} = _Request, _From, State) ->
  % it's not allowed to unregister `unique_name()'
  {reply, {error, badarg}, State};

handle_call({unregister, PipePid, Name} = _Request, _From, State) ->
  ets:delete_object(name_to_pid, {Name, PipePid}),
  ets:delete_object(pid_to_name, {Name, PipePid}),
  % NOTE: it should never happen that a stream unregistered all its names (it
  % shouldn't unregister its `unique_name()'), so don't unmonitor it yet
  {reply, {ok, subscribers(PipePid)}, State};

handle_call({unregister, PipePid} = _Request, _From, State) ->
  shutdown_monitor(stream, PipePid),

  % AffectedStreamNames :: {stream_name(), Pipeline :: pid()}
  AffectedStreamNames = ets:lookup(pid_to_name, PipePid),
  [ets:delete_object(name_to_pid, S) || S <- AffectedStreamNames],
  ets:delete(pid_to_name, PipePid),

  % remove all subscriptions to this unique name (see `subscribe/1')
  case [U || {"U:" ++ _ = U, _} <- AffectedStreamNames] of
    [UniqueName] ->
      % there should be exactly one unique name for this stream
      UniqueNameSubscribers = ets:lookup(name_to_subscriber, UniqueName),
      [ets:delete_object(subscriber_to_name, S) || S <- UniqueNameSubscribers],
      ets:delete(name_to_subscriber, UniqueName),
      {reply, ok, State};
    [] ->
      % if it was not a registered stream (somebody else's error), it should
      % not crash
      {reply, {error, badarg}, State}
      % more than one unique name is internal error and can (should?) lead to
      % crash
  end;

%% }}}
%%----------------------------------------------------------
%% stream subscription {{{

%% subscription request to a `unique_name()'
handle_call({subscribe, Subscriber, "U:" ++ _ = Name} = _Request, _From,
            State) ->
  case ets:member(name_to_pid, Name) of
    true ->
      ets:insert(name_to_subscriber, {Name, Subscriber}),
      ets:insert(subscriber_to_name, {Name, Subscriber}),
      send_subscribers(Name),
      {reply, ok, State};
    false ->
      {reply, {error, no_stream}, State}
  end;

%% subscription request to a `well_known_name()'
handle_call({subscribe, Subscriber, Name} = _Request, _From, State) ->
  case correct_well_known_names([Name]) of
    true ->
      setup_monitor(subscriber, Subscriber),
      ets:insert(name_to_subscriber, {Name, Subscriber}),
      ets:insert(subscriber_to_name, {Name, Subscriber}),
      send_subscribers(Name),
      {reply, ok, State};
    false ->
      {reply, {error, badarg}, State}
  end;

handle_call({unsubscribe, Subscriber, Name} = _Request, _From, State) ->
  ets:delete_object(name_to_subscriber, {Name, Subscriber}),
  ets:delete_object(subscriber_to_name, {Name, Subscriber}),
  send_subscribers(Name),
  % if no subscriptions, no need to keep Subscriber monitored
  case ets:member(subscriber_to_name, Subscriber) of
    true  -> ok;
    false -> shutdown_monitor(subscriber, Subscriber)
  end,
  {reply, ok, State};

handle_call({unsubscribe, Subscriber} = _Request, _From, State) ->
  AffectedStreamNames = ets:lookup(subscriber_to_name, Subscriber),
  [ets:delete_object(name_to_subscriber, S) || S <- AffectedStreamNames],
  ets:delete(subscriber_to_name, Subscriber),
  % send updated list of subscribers to affected streams
  AffectedStreamPidsDuplicates = [
    Pid ||
      {Name,_} <- AffectedStreamNames,
      {_,Pid} <- ets:lookup(name_to_pid, Name)
  ],
  [send_subscribers(Pid) || Pid <- lists:usort(AffectedStreamPidsDuplicates)],
  shutdown_monitor(subscriber, Subscriber),
  {reply, ok, State};

%% }}}
%%----------------------------------------------------------
%% inspecting stream <-> name mappings {{{

handle_call({names_of, PipePid} = _Request, _From, State) ->
  Result = [Name || {Name,_Pid} <- ets:lookup(pid_to_name, PipePid)],
  {reply, Result, State};

handle_call({pids_of, Name} = _Request, _From, State) ->
  Result = [Pid || {_Name,Pid} <- ets:lookup(name_to_pid, Name)],
  {reply, Result, State};

%% }}}
%%----------------------------------------------------------

%% unknown call -- ignore
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%% @private
%% @doc Handle {@link gen_server:cast/2}.

%% unknown cast -- ignore
handle_cast(_Request, State) ->
  {noreply, State}.

%% @private
%% @doc Handle incoming messages.

%%----------------------------------------------------------
%% monitor messages {{{

handle_info({'DOWN', _Ref, process, Pid, _Reason} = _Message, State) ->
  % FIXME: cheating! I should move these to separate functions rather than
  % just calling gen_server callbacks directly
  % XXX: first, unregister the process, then unsubscribe
  handle_call({unregister,  Pid}, ignore, State), % this doesn't change State
  handle_call({unsubscribe, Pid}, ignore, State), % this doesn't change State
  {noreply, State};

%% }}}
%%----------------------------------------------------------

%% unknown message -- ignore
handle_info(_Message, State) ->
  {noreply, State}.

%% @private
%% @doc Handle code change notification.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%---------------------------------------------------------------------------
%%% helpers for public API {{{

%% @doc Generate unique name for a stream.
%%
%% @spec generate_name() ->
%%   unique_name()

generate_name() ->
  "U:" ++ streem_unique:hex_string().

%% @doc Check if each of specified names is {@type well_known_name()}.
%%
%% @spec correct_well_known_names([string()]) ->
%%   boolean()

-spec correct_well_known_names([string()]) ->
  boolean().

correct_well_known_names(Names) ->
  {ok, Re} = re:compile("^[a-zA-Z0-9_./@-]+$", [dollar_endonly]),
  NonMatching = [N || N <- Names, re:run(N, Re, [{capture, none}]) /= match],
  % if `NonMatching' is empty, every name is correct
  NonMatching == [].

%% @doc Send updated list of subscribers to streams specified by
%%   {@type stream_name()} or by {@type pid()}.

-spec send_subscribers(stream_name() | pid()) ->
  ok.

send_subscribers(PipePid) when is_pid(PipePid) ->
  PipePid ! {subscribers,subscribers(PipePid)},
  ok;

send_subscribers(StreamName) ->
  [PipePid ! {subscribers,subscribers(PipePid)} ||
    {_Name,PipePid} <- ets:lookup(name_to_pid, StreamName)],
  ok.

%% @doc Build list of subscribers for specified pipeline.

-spec subscribers(pid()) ->
  subscribers().

subscribers(PipePid) ->
  Subscribers = [SubPid ||
    {Name,_PipePid} <- ets:lookup(pid_to_name, PipePid),
    {_Name,SubPid} <- ets:lookup(name_to_subscriber, Name)],
  lists:usort(Subscribers).

%% @doc Setup monitoring specified process.
%%   No-op if the process is already monitored.

setup_monitor(Type, Pid) ->
  Key = {Type, Pid},
  case ets:member(monitor, Key) of
    true ->
      ok;
    false ->
      Ref = erlang:monitor(process, Pid),
      ets:insert(monitor, {Key, Ref}),
      ok
  end.

%% @doc Disable monitoring specified process.

shutdown_monitor(Type, Pid) ->
  Key = {Type, Pid},
  case ets:lookup(monitor, Key) of
    [] ->
      ok;
    [{Key, Ref}] ->
      erlang:demonitor(Ref, [flush]),
      ets:delete(monitor, Key),
      ok
  end.

%%% }}}
%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
