%%%---------------------------------------------------------------------------
%%% @private
%%% @doc
%%%   Streem top supervisor.
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_sup).

-behaviour(supervisor).

%% starting process
-export([start_link/0]).

%% supervisor callbacks
-export([init/1]).

-define(SUP_LISTENER_CHILD(ID, AddrSpec),
  {ID,
    {streem_sjcp_sup, start_link, [AddrSpec]},
    permanent, infinity, supervisor, [streem_sjcp_sup]}
).

%%%---------------------------------------------------------------------------
%%% starting process
%%%---------------------------------------------------------------------------

%% @doc Start supervisor process.

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%---------------------------------------------------------------------------
%%% supervisor callbacks
%%%---------------------------------------------------------------------------

%% @doc Return tree specification.

init(_Args) ->
  Strategy = {one_for_all, 5, 10},
  {ok, Listen} = application:get_env(streem, listen),
  SupervisorChildren = [?SUP_LISTENER_CHILD(make_ref(), A) || A <- Listen],

  WorkerChildren = [
    {registry,
      {streem_registry, start_link, []},
      permanent, 3000, worker, [streem_registry]},
    {query_manager,
      {streem_query_manager, start_link, []},
      permanent, 3000, worker, [streem_query_manager]}
  ],
  {ok, {Strategy, SupervisorChildren ++ WorkerChildren}}.

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
