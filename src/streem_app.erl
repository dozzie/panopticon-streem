%%%---------------------------------------------------------------------------
%%% @doc
%%%   Streem application.
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_app).

-behaviour(application).

%% application callbacks
-export([start/2, stop/1]).

%%%---------------------------------------------------------------------------
%%% application callbacks
%%%---------------------------------------------------------------------------

%% @doc Start the application.

start(_StartType, _Args) ->
  streem_sup:start_link().

%% @doc Stop the application.

stop(_State) ->
  ok.

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
