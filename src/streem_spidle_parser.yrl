%%%---------------------------------------------------------------------------

Header
  "%%% @private"
.

Nonterminals
  pipe_definition
  operation stream_split select tee
.

Terminals
  '|' ',' '+' '(' ')'
  string
.

Rootsymbol pipe_definition.

%%%---------------------------------------------------------------------------
%%% TODO: whitespaces

pipe_definition -> operation '|' pipe_definition : ['$1' | '$3'].
pipe_definition -> operation                     : ['$1'].

operation -> stream_split : '$1'.
operation -> string       : value('$1').

stream_split -> '(' pipe_definition ')' select : {select, ['$2' | '$4']}.
stream_split -> '(' pipe_definition ')' tee    : {tee,    ['$2' | '$4']}.

select -> ',' '(' pipe_definition ')' select : ['$3' | '$5'].
select -> ',' '(' pipe_definition ')'        : ['$3'].

tee -> '+' '(' pipe_definition ')' tee : ['$3' | '$5'].
tee -> '+' '(' pipe_definition ')'     : ['$3'].

%%%---------------------------------------------------------------------------

Erlang code.

%terminal({TermName, _Line}) ->
%  TermName;
%terminal({TermName, _Line, _Value}) ->
%  TermName.

value({_TermName, _Line, Value}) ->
  Value.

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang
