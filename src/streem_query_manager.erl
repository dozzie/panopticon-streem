%%%---------------------------------------------------------------------------
%%% @doc
%%%   Stream query manager process. The process is responsible for parsing the
%%%   query, spawning query executor ({@link streem_pipeline}) and registering
%%%   its names ({@link streem_registry}).
%%%
%%%   This module is the place to put any knowledge about conversion from
%%%   string to Erlang term suitable for `init/1' ({@link gen_streem_op})
%%%   modules.
%%%
%%%   === `map' syntax ===
%%%
%%%   <ul>
%%%     <li>`map "key name" = "static string value"' -- set to key to static
%%%         value</li>
%%%     <li>`map "key name" = 10' -- ditto, but to integer</li>
%%%     <li>`map "key name" = null' -- ditto, to `null'</li>
%%%     <li>`map unset "key to remove"' -- remove key from hash
%%%         altogether</li>
%%%     <li>`map "old key" -> "new key"' -- rename a key</li>
%%%   </ul>
%%%
%%%   === `filter' syntax ===
%%%
%%%   In `filter', key name is always the left one.
%%%
%%%   <ul>
%%%     <li>`filter "key name" ~ "^\.regexp"' -- regexp match; the key must
%%%         exist and must be a string; regexp inside the quotes doesn't
%%%         require any additional quoting (the example matches period)</li>
%%%     <li>`filter "key name" = null' -- check the key for presence, but
%%%         being set to `null'</li>
%%%     <li>`filter "key name" != null' -- check the key for presence, but for
%%%         being different from `null'</li>
%%%     <li>`filter "key name" < 10' -- key must exist, be a number (float is
%%%         OK) and be lower than 10</li>
%%%     <li>`filter "key name"' -- check the key for presence</li>
%%%   </ul>
%%%
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_query_manager).

-behaviour(gen_server).

%% public API
-export([run/1, run/2, stop/1, list/0]).
-export([spidle/1, pid_of/1, qid_of/1]).

%% starting process
-export([start/0, start_link/0]).

%% gen_server callbacks
-export([init/1, terminate/2]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-export([code_change/3]).

%% query parsing
-export([parse/1]).

-record(state, {
  pids = dict:new(),    % PID -> QID
  qids = dict:new(),    % QID -> PID
  spidle  = dict:new(), % QID -> Spidle
  monitor = dict:new()  % PID -> monitor reference
}).

%%%---------------------------------------------------------------------------
%%% regexp macros
%%%
%%% These regexps are for very, very tricky thing. I'll use their capture
%%% groups later (`args()' function) to check which one matched, so I can
%%% discriminate between setting a variable to an int, float or a string.
%%%
%%% This code is dangerous, don't do it at home!

%%----------------------------------------------------------
%% data types (common to `filter' and `map') {{{

%% no capture groups
-define(STRING, "\"(?:[^\\\\\"]|\\\\.)*\"|'(?:[^\\\\']|\\\\.)*'").
%% 2 capture groups for floats
-define(NUMBER, "-?(?:[1-9][0-9]*|0)(\\.[0-9]+)?([eE][+-]?[0-9]+)?").
-define(BOOL,   "true|false").
-define(NULL,   "null").

%% }}}
%%----------------------------------------------------------
%% `map' operation {{{

%% [Key, StringVal, NumberVal, _IsFloat1, _IsFloat2, Constant]
-define(RE_SET, "(" ?STRING ")\\s*=\\s*(?:(" ?STRING ")|(" ?NUMBER ")|"
                                         "(" ?BOOL "|" ?NULL "))").
% [Key]
-define(RE_UNSET, "unset\\s*(" ?STRING ")").
% [OldKey, NewKey]
-define(RE_RENAME, "(" ?STRING ")\\s*->\\s*(" ?STRING ")").

%% [_All,
%%   _Unset, Key,
%%   _Rename, OldKey, NewKey, 
%%   _Set, Key, StringVal, NumberVal, _IsFloat1, _IsFloat2, Constant]
-define(RE_MAP, "^\\s*(?:(" ?RE_UNSET ")|(" ?RE_RENAME ")|"
                        "(" ?RE_SET "))\\s*$").

%% }}}
%%----------------------------------------------------------
%% `filter' operation {{{

-define(CMP, "<=?|>=?|!?=|~").

%% NOTE: The regexp below allows `"foo" ~ null', but disallowing such a silly
%% construct would be very troublesome, while this code is to be thrown away
%% in favour of fully-blown Spidle parser.

%% [_All, Key, Cmp, StringVal, NumberVal, _IsFloat1, _IsFloat2, Constant]
-define(RE_FILTER, "^\\s*(?:(" ?STRING ")\\s*"
                           "(?:(" ?CMP ")\\s*"
                              "(?:(" ?STRING ")|(" ?NUMBER ")|"
                                 "(" ?BOOL "|" ?NULL ")))?)\\s*$").

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% types

-type spidle_query() :: string().

-type query_id() :: streem_registry:unique_name().

%%%---------------------------------------------------------------------------
%%% public API
%%%---------------------------------------------------------------------------

%% @doc Execute an unnamed Spidle query.
%%
%% @spec run(spidle_query()) ->
%%   {ok, query_id(), pid()} | {error, Reason}

-spec run(spidle_query()) ->
  {ok, {query_id(), pid()}} | {error, term()}.

run(Query) ->
  gen_server:call(?MODULE, {run, Query}).

%% @doc Execute a Spidle query, registering it under specified names in
%%   {@link streem_registry}.
%%
%% @spec run(spidle_query(), [streem_registry:well_known_name()]) ->
%%   {ok, query_id(), pid()} | {error, Reason}

-spec run(spidle_query(), [streem_registry:well_known_name()]) ->
  {ok, {query_id(), pid()}} | {error, term()}.

run(Query, Names) ->
  gen_server:call(?MODULE, {run, Query, Names}).

%% @doc Stop query.
%%
%% @spec stop(query_id()) ->
%%   ok

-spec stop(query_id()) ->
  ok.

stop(QID) ->
  gen_server:call(?MODULE, {stop, QID}).

%% @doc List queries.
%%
%% @spec list() ->
%%   [query_id()]

-spec list() ->
  [query_id()].

list() ->
  gen_server:call(?MODULE, {list}).

%% @doc Retrieve Spidle string used to run the query.
%%
%% @spec spidle(query_id()) ->
%%   spidle_query() | undefined

-spec spidle(query_id()) ->
  spidle_query() | undefined.

spidle(QID) ->
  gen_server:call(?MODULE, {spidle, QID}).

%% @doc Retrieve {@type pid()} of a process executing the query.
%%
%% @spec pid_of(query_id()) ->
%%   pid() | undefined

-spec pid_of(query_id()) ->
  pid() | undefined.

pid_of(QID) ->
  gen_server:call(?MODULE, {pid_of, QID}).

%% @doc Retrieve {@type query_id()} of a process which executes a query.
%%
%% @spec qid_of(pid()) ->
%%   query_id() | undefined

-spec qid_of(pid()) ->
  query_id() | undefined.

qid_of(Pid) ->
  gen_server:call(?MODULE, {qid_of, Pid}).

%%%---------------------------------------------------------------------------
%%% starting process
%%%---------------------------------------------------------------------------

%% @doc Start manager process.
%%
%%   Function registers the process locally, so only one can be started in
%%   Erlang VM.
%%
%% @spec start() ->
%%   {ok, pid()} | {error, Reason}

-spec start() ->
  {ok, pid()} | {error, term()}.

start() ->
  gen_server:start({local, ?MODULE}, ?MODULE, none, []).

%% @doc Start (and link) manager process.
%%
%%   Function registers the process locally, so only one can be started in
%%   Erlang VM.
%%
%% @spec start_link() ->
%%   {ok, pid()} | {error, Reason}

-spec start_link() ->
  {ok, pid()} | {error, term()}.

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).

%%%---------------------------------------------------------------------------
%%% gen_server callbacks
%%%---------------------------------------------------------------------------

%% @private
%% @doc Manager process initialization.

init(_Args) ->
  {ok, #state{}}.

%% @private
%% @doc Process cleanup.

terminate(_Reason, _State) ->
  ok.

%% @private
%% @doc Handle {@link gen_server:call/2}.

%%----------------------------------------------------------
%% public API handlers {{{

%% execute unnamed query
handle_call({run, Query} = _Request, _From, State) ->
  case parse(Query) of
    {ok, Nodes} ->
      % TODO: supervision tree
      % FIXME: temporary hack for `source' construct
      {Sources, PipeDef} = split_nodes(Nodes),
      {ok, Pid} = streem_pipeline:start(PipeDef),
      [streem_registry:subscribe(Pid, S) || S <- Sources],
      % each stream should have at least `streem_registry:unique_name()'
      {ok, Subscribers} = streem_registry:register(Pid, []),
      [QID] = streem_registry:names_of(Pid),
      % subscribers list should be empty, but just in case
      Pid ! {subscribers, Subscribers},
      {reply, {ok, {QID, Pid}}, add_query(Pid, QID, Query, State)};
    {error, _Reason} = Error ->
      {reply, Error, State}
  end;

%% execute named query
handle_call({run, Query, Names} = _Request, _From, State) ->
  case parse(Query) of
    {ok, Nodes} ->
      % TODO: supervision tree
      % FIXME: temporary hack for `source' construct
      {Sources, PipeDef} = split_nodes(Nodes),
      {ok, Pid} = streem_pipeline:start(PipeDef),
      [streem_registry:subscribe(Pid, S) || S <- Sources],
      % at the beginning, register with empty names list, what results in
      % having exactly one name (`streem_registry:unique_name()');
      % subscribers returned here don't matter, since the stream will be
      % registered again below
      {ok, _} = streem_registry:register(Pid, []),
      [QID] = streem_registry:names_of(Pid),
      % now that QID is retrieved, register all the names
      {ok, Subscribers} = streem_registry:register(Pid, Names),
      % somebody could already subscribe to one of the stream names
      Pid ! {subscribers, Subscribers},
      {reply, {ok, {QID, Pid}}, add_query(Pid, QID, Query, State)};
    {error, _Reason} = Error ->
      {reply, Error, State}
  end;

handle_call({stop, QID} = _Request, _From, State = #state{qids = Qids}) ->
  NewState = case dict:find(QID, Qids) of
    {ok, Pid} ->
      streem_pipeline:stop(Pid),
      delete_query(Pid, QID, State);
    error ->
      State
  end,
  {reply, ok, NewState};

handle_call({list} = _Request, _From, State = #state{qids = Qids}) ->
  Result = dict:fetch_keys(Qids),
  {reply, Result, State};

handle_call({spidle, QID} = _Request, _From,
            State = #state{spidle = Queries}) ->
  case dict:find(QID, Queries) of
    {ok, Query} -> {reply, Query, State};
    error       -> {reply, undefined, State}
  end;

handle_call({pid_of, QID} = _Request, _From, State = #state{qids = Qids}) ->
  case dict:find(QID, Qids) of
    {ok, Pid} -> {reply, Pid, State};
    error     -> {reply, undefined, State}
  end;

handle_call({qid_of, Pid} = _Request, _From, State = #state{pids = Pids}) ->
  case dict:find(Pid, Pids) of
    {ok, QID} -> {reply, QID, State};
    error     -> {reply, undefined, State}
  end;

%% }}}
%%----------------------------------------------------------

%% unknown call -- ignore
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%% @private
%% @doc Handle {@link gen_server:cast/2}.

%% unknown cast -- ignore
handle_cast(_Request, State) ->
  {noreply, State}.

%% @private
%% @doc Handle incoming messages.

%% query is down
handle_info({'DOWN', _Ref, process, Pid, _Reason} = _Message, State) ->
  {noreply, delete_query(Pid, State)};

%% unknown message -- ignore
handle_info(_Message, State) ->
  {noreply, State}.

%% @private
%% @doc Handle code change notification.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%---------------------------------------------------------------------------
%%% state (`#state{}') manipulation
%%%---------------------------------------------------------------------------

%% @doc Add query (identified by PID and QID) to {@link gen_server} state.
%%
%% @spec add_query(pid(), query_id(), spidle_query(), #state{}) ->
%%   #state{}

add_query(Pid, QID, Query, State = #state{}) ->
  Ref = erlang:monitor(process, Pid),
  State#state{
    pids = dict:store(Pid, QID, State#state.pids),
    qids = dict:store(QID, Pid, State#state.qids),
    spidle  = dict:store(QID, Query, State#state.spidle),
    monitor = dict:store(Pid, Ref,   State#state.monitor)
  }.

%% @doc Remove query (identified by PID <b>and</b> QID) from {@link gen_server}
%%   state.
%%
%% @spec delete_query(pid(), query_id(), #state{}) ->
%%   #state{}

delete_query(Pid, QID, State = #state{}) ->
  case dict:find(Pid, State#state.monitor) of
    {ok, Ref} -> erlang:demonitor(Ref, [flush]);
    error -> ok
  end,
  State#state{
    pids = dict:erase(Pid, State#state.pids),
    qids = dict:erase(QID, State#state.qids),
    spidle  = dict:erase(QID, State#state.spidle),
    monitor = dict:erase(Pid, State#state.monitor)
  }.

%% @doc Remove query (identified by either PID or QID) from {@link gen_server}
%%   state.
%%
%% @spec delete_query(pid() | query_id(), #state{}) ->
%%   #state{}

delete_query(_ID = Pid, State = #state{pids = Pids}) when is_pid(Pid) ->
  case dict:find(Pid, Pids) of
    {ok, QID} -> delete_query(Pid, QID, State);
    error     -> State
  end;
delete_query(_ID = QID, State = #state{qids = Qids}) when is_list(QID) ->
  case dict:find(QID, Qids) of
    {ok, Pid} -> delete_query(Pid, QID, State);
    error     -> State
  end.

%%%---------------------------------------------------------------------------
%%% query parsing
%%%---------------------------------------------------------------------------
%%%
%%% XXX HERE BE DRAGONS! XXX
%%%
%%% At least for now, until it's moved to Spidle parser.
%%%

%% @private
%% @doc Parse Spidle query to pipe definition usable with
%%   {@link streem_pipeline:init/1}.

parse(Query) ->
  case streem_spidle_lexer:string(Query) of
    {ok, Tokens, _LineNo} ->
      case streem_spidle_parser:parse(Tokens) of
        {ok, NodeDefs} ->
          {ok, string_to_args(NodeDefs)};
        {error, _Reason} ->
          {error, parse_error}
      end;
    {error, _Reason, _LineNo} ->
      {error, parse_error}
  end.

%% @doc Convert strings from parser into arguments suitable for
%%   {@link streem_pipeline:init/1}.
%%
%%   This function is the exact place where string->gen_streem_op
%%   transformation knowledge is embedded.

string_to_args([] = _Nodes) ->
  [];

string_to_args([Mod | Rest] = _Nodes) when is_list(Mod) ->
  NameLen = string:cspan(Mod, " \t"),
  Name = string:substr(Mod, 1, NameLen),
  Args = string:strip(string:substr(Mod, NameLen + 1)),

  % XXX: hardcoded mapping
  % TODO: error reporting
  Node = case Name of
    "map"    -> {op, streem_op_map,    args(map, Args)};
    "filter" -> {op, streem_op_filter, args(filter, Args)};
    "grep"   -> {op, streem_op_filter, args(filter, Args)};
    "source" -> {source, Args}
  end,

  [Node | string_to_args(Rest)];

string_to_args([{select, SubPipe} | Rest] = _Nodes) ->
  Node = {select, [string_to_args(P) || P <- SubPipe]},
  [Node | string_to_args(Rest)];

string_to_args([{tee, SubPipe} | Rest] = _Nodes) ->
  Node = {tee, [string_to_args(P) || P <- SubPipe]},
  [Node | string_to_args(Rest)].

%% @doc Parse string to arguments for a module.
%%
%% @TODO Embed all this knowledge about syntax in Spidle parser.

args(map, String) ->
  % check if the string matches (`nomatch' -> boom!)
  % if it does, check which capture groups are defined, and based on this
  % knowledge, run parser for int, float, string or other
  case re:run(String, ?RE_MAP) of
    {match, [_All, _Unset, Key]} ->
      {unset, string(String, Key)};

    {match, [_All, {-1,0}, {-1,0}, _Rename, OldKey, NewKey]} ->
      {rename, string(String, OldKey), string(String, NewKey)};

    {match, [_All, {-1,0}, {-1,0}, {-1,0}, {-1,0}, {-1,0},
              _Set, Key, StringVal]} ->
      {set, string(String, Key), {value, string(String, StringVal)}};

    {match, [_All, {-1,0}, {-1,0}, {-1,0}, {-1,0}, {-1,0},
              _Set, Key, {-1,0}, NumberVal]} ->
      {set, string(String, Key), {value, integer(String, NumberVal)}};

    {match, [_All, {-1,0}, {-1,0}, {-1,0}, {-1,0}, {-1,0},
              _Set, Key, {-1,0}, NumberVal, _IsFloat]} ->
      {set, string(String, Key), {value, float(String, NumberVal)}};

    {match, [_All, {-1,0}, {-1,0}, {-1,0}, {-1,0}, {-1,0},
              _Set, Key, {-1,0}, NumberVal, _, _IsFloat]} ->
      {set, string(String, Key), {value, float(String, NumberVal)}};

    {match, [_All, {-1,0}, {-1,0}, {-1,0}, {-1,0}, {-1,0},
              _Set, Key, {-1,0}, {-1,0}, {-1,0}, {-1,0}, Constant]} ->
      {set, string(String, Key), {value, const(String, Constant)}}
  end;

args(filter, String) ->
  % check if the string matches (`nomatch' -> boom!)
  % if it does, check which capture groups are defined, and based on this
  % knowledge, run parser for int, float, string or other
  case re:run(String, ?RE_FILTER) of
    {match, [_All, Key]} ->
      {string(String, Key), exists};

    {match, [_All, Key, Cmp, StringVal]} ->
      {string(String, Key), op(String, Cmp), string(String, StringVal)};

    {match, [_All, Key, Cmp, {-1,0}, NumberVal]} ->
      {string(String, Key), op(String, Cmp), integer(String, NumberVal)};

    {match, [_All, Key, Cmp, {-1,0}, NumberVal, _IsFloat]} ->
      {string(String, Key), op(String, Cmp), float(String, NumberVal)};

    {match, [_All, Key, Cmp, {-1,0}, NumberVal, _, _IsFloat]} ->
      {string(String, Key), op(String, Cmp), float(String, NumberVal)};

    {match, [_All, Key, Cmp, {-1,0}, {-1,0}, {-1,0}, {-1,0}, Constant]} ->
      {string(String, Key), op(String, Cmp), const(String, Constant)}
  end.

%% @doc Convert string-match for a comparison operator to appropriate atom.

op(String, {Offset, Length}) ->
  case string:substr(String, Offset + 1, Length) of
    "<"  -> '<';
    "<=" -> '<=';
    ">"  -> '>';
    ">=" -> '>=';
    "="  -> '=';
    "!=" -> '!=';
    "~"  -> regexp
  end.

%% @doc Convert string-match for constant (`true', `false', `null') to
%%   appropriate atom.

const(String, {Offset, Length}) ->
  case string:substr(String, Offset + 1, Length) of
    "true"  -> true;
    "false" -> false;
    "null"  -> null
  end.

%% @doc Convert string-match for integer to {@type integer()}.

integer(String, {Offset, Length}) ->
  list_to_integer(string:substr(String, Offset + 1, Length)).

%% @doc Convert string-match for float to {@type float()}.

float(String, {Offset, Length}) ->
  list_to_float(string:substr(String, Offset + 1, Length)).

%% @doc Convert string-match for string to {@type binary()}.
%%
%% @TODO Support for "`\X'" sequences.

string(String, {Offset, Length}) ->
  % TODO: make it more effective
  % TODO: support for "\X" sequences
  S = string:substr(String, Offset + 1, Length),
  S1 = string:strip(string:strip(S, both, $'), both, $"),
  list_to_binary(S1).

%%%---------------------------------------------------------------------------

%% @doc Split nodes into two sets: message sources and processing nodes.
%%
%% @TODO This function is a temporary hack for `source' construct.

split_nodes([] = _Nodes) ->
  {[], []};
split_nodes([{source, Name} | Rest] = _Nodes) ->
  {Sources, ProcessingNodes} = split_nodes(Rest),
  {[Name | Sources], ProcessingNodes};
split_nodes([N | Rest] = _Nodes) ->
  {Sources, ProcessingNodes} = split_nodes(Rest),
  {Sources, [N | ProcessingNodes]}.

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
