%%%---------------------------------------------------------------------------
%%% @doc
%%%   Single operation on message behaviour.
%%%
%%%
%%%   An operation is required to provide following functions:
%%%   <ul>
%%%     <li>`process_message(Message, State)', which returns:
%%%       <ul>
%%%         <li>`{accept,NewMessage,NewState}'</li>
%%%         <li>`{split,Messages,NewState}', where `Messages' is a list of
%%%             {@type streem_pipeline:message()}</li>
%%%         <li>`reject'</li>
%%%         <li>`{consume,NewState}'</li>
%%%       </ul>
%%%     </li>
%%%     <li>`init(Args) -> {ok,State}'</li>
%%%     <li>`terminate(State) -> any()'</li>
%%%   </ul>
%%%
%%%   `Message' is a tuple of `{Data,Meta}'. `Meta' is unused for now.
%%% @end
%%%---------------------------------------------------------------------------

-module(gen_streem_op).

-export([behaviour_info/1]).

%%%---------------------------------------------------------------------------

%% @doc Behaviour description.
behaviour_info(callbacks = _Aspect) ->
  [{init, 1}, {terminate, 1},
    {process_message, 2}];
behaviour_info(_Aspect) ->
  undefined.

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
