%%%---------------------------------------------------------------------------
%%% @private
%%% It's a pity that this marker won't work.
%%%---------------------------------------------------------------------------

Definitions.

SP = [\s\t]+
STRING = "([^\\"]|\\.)*"|'([^\\']|\\.)*'
NON_SPECIAL_CHAR = [^"'|,+()\s\t]+

OP_TOKEN = ({STRING}|{NON_SPECIAL_CHAR})+

%%%---------------------------------------------------------------------------

Rules.

\| : {token, {'|', TokenLine}}.
,  : {token, {',', TokenLine}}.
\+ : {token, {'+', TokenLine}}.

\( : {token, {'(', TokenLine}}.
\) : {token, {')', TokenLine}}.

{OP_TOKEN}({SP}{OP_TOKEN})* : {token, {string, TokenLine, TokenChars}}.

%% free-floating spaces (around '|', '(', ')', ',', '+')
%% TODO: support for `foo(x)' queries
{SP} : skip_token.

%%%---------------------------------------------------------------------------

Erlang code.

%%%---------------------------------------------------------------------------
