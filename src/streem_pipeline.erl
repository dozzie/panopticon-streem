%%%---------------------------------------------------------------------------
%%% @doc
%%%   Module representing whole pipeline.
%%%
%%% @TODO Handling errors in operation modules.
%%% @TODO Support for producing messages out of nothing in the middle of
%%%   pipeline (messages coming from other pipelines, so called <i>select</i>
%%%   operations).
%%% @TODO Unify handling external and direct subscribers.
%%%
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_pipeline).

-behaviour(gen_server).

%% messages
-export([send/2, send/3, send_async/2]).
-export_type([message/0, payload/0, metadata/0]).

%% pipeline management
-export([init_pipeline/1, terminate_pipeline/1, process_message/2]).

%% subscription management
-export([subscribe/1, subscribe/2, unsubscribe/1, unsubscribe/2]).

%% starting process
-export([start/1, start_link/1, stop/1]).

%% gen_server callbacks
-export([init/1, terminate/2]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-export([code_change/3]).

%% Direct subscribers are the ones who subscribed to this pipe. Those are
%% monitored. External ones come from registry and are not monitored.
-record(state, {
  pipe,
  direct_subscribers   = [],
  external_subscribers = []
}).

%%%---------------------------------------------------------------------------
%%% types

%%----------------------------------------------------------
%% messages {{{

%% @type message() = {payload(), metadata()}.
%%   Message to process, along with its payload.

-type message() :: {payload(), metadata()}.

%% @type payload() = term().
%%   Body of a message.

-type payload() :: term().

%% @type metadata() = list().
%%   Metadata (e.g. variables) attached to a {@type message()}.

-type metadata() :: list().

%% }}}
%%----------------------------------------------------------
%% pipeline structure {{{

%% @type pipeline() = [operation()].

-type pipeline() :: [operation()].

%% @type operation() = {op, Module :: atom(), OpState :: term()} | subpipe().

-type operation() :: {op, atom(), term()} | subpipe().

%% @type subpipe() = {select, [pipeline()]} | {tee, [pipeline()]}.

-type subpipe() :: {select, [pipeline()]} | {tee, [pipeline()]}.

%% }}}
%%----------------------------------------------------------

%% @type pipeline_handle() = pid().

-type pipeline_handle() :: pid().

%% @type pipeline_definition() = list().

-type pipeline_definition() :: list().

%%%---------------------------------------------------------------------------
%%% messages
%%%---------------------------------------------------------------------------

%%----------------------------------------------------------
%% sending the messages {{{

%% @doc Submit a message to pipeline.
%%   The call is synchronous.
%%
%%   <b>NOTE</b>: Currently, "synchronous" means "wait until the pipe
%%   processes the message".
%%
%% @spec send(pipeline_handle(), payload()) ->
%%   ok

-spec send(pipeline_handle(), payload()) ->
  ok.

send(PipePid, Message) ->
  send(PipePid, Message, infinity).

%% @doc Submit a message to pipeline.
%%   The call is synchronous.
%%
%%   <b>NOTE</b>: Currently, "synchronous" means "wait until the pipe
%%   processes the message".
%%
%% @spec send(pipeline_handle(), payload(), integer() | infinity) ->
%%   ok

-spec send(pipeline_handle(), payload(), integer() | infinity) ->
  ok.

send(PipePid, Message, Timeout) ->
  gen_server:call(PipePid, {message, Message}, Timeout).

%% @doc Submit a message to pipeline (asynchronously).
%%
%% @spec send_async(pipeline_handle(), payload()) ->
%%   ok

-spec send_async(pipeline_handle(), payload()) ->
  ok.

send_async(PipePid, Message) ->
  PipePid ! {message, Message},
  ok.

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% pipeline management
%%%---------------------------------------------------------------------------

%%----------------------------------------------------------
%% pipeline initialization {{{

%% @doc Initialize modules' states according to definitions.
%%
%% @spec init_pipeline(pipeline_definition()) ->
%%   pipeline()

-spec init_pipeline(pipeline_definition()) ->
  pipeline().

init_pipeline([] = _Modules) ->
  [];

init_pipeline([{op, Module, Args} | Rest] = _Modules) ->
  {ok, State} = Module:init(Args),
  Node = {op, Module, State},
  [Node | init_pipeline(Rest)];

init_pipeline([{select, SubPipe} | Rest] = _Modules) ->
  Node = {select, [init_pipeline(P) || P <- SubPipe]},
  [Node | init_pipeline(Rest)];

init_pipeline([{tee, SubPipe} | Rest] = _Modules) ->
  Node = {tee, [init_pipeline(P) || P <- SubPipe]},
  [Node | init_pipeline(Rest)].

%% }}}
%%----------------------------------------------------------
%% pipeline termination {{{

%% @doc Terminate pipeline.
%%
%% @spec terminate_pipeline(pipeline()) ->
%%   ok

-spec terminate_pipeline(pipeline()) ->
  ok.

terminate_pipeline([] = _Pipe) ->
  ok;

terminate_pipeline([{op, Module, State} | Rest] = _Pipe) ->
  Module:terminate(State),
  terminate_pipeline(Rest);

terminate_pipeline([{select, SubPipes} | Rest] = _Pipe) ->
  [terminate_pipeline(P) || P <- SubPipes],
  terminate_pipeline(Rest);

terminate_pipeline([{tee, SubPipes} | Rest] = _Pipe) ->
  [terminate_pipeline(P) || P <- SubPipes],
  terminate_pipeline(Rest).

%% }}}
%%----------------------------------------------------------
%% executing pipeline on a message {{{

%% @doc Process a message according to pipeline definition.
%%
%% @spec process_message(message(), pipeline()) ->
%%   {Accepted :: [message()], Rejected :: [message()]}
%%
%% @TODO Honouring state changes for operations.

-spec process_message(message(), pipeline()) ->
  {Accepted :: [message()], Rejected :: [message()]}.

%% end of pipeline, message can be considered accepted
process_message(Msg, [] = _Pipe) ->
  {[Msg], []};

%% single `gen_streem_op' operation
process_message(Msg, [{op, Mod, State} | Rest] = _Pipe) ->
  case Mod:process_message(Msg, State) of
    {accept, NewMsg, _NewState} ->              % TODO: honour state change
      % message was successfully processed, pass it further
      process_message(NewMsg, Rest);
    {split, Messages, _NewState} ->             % TODO: honour state change
      % message was successfully processed -- and split, pass results further
      Processed = [process_message(M, Rest) || M <- Messages],
      Accepted = [A || {AL,_RL} <- Processed, A <- AL],
      Rejected = [R || {_AL,RL} <- Processed, R <- RL],
      {Accepted, Rejected};
    {consume, _NewState} ->                     % TODO: honour state change
      % message was consumed, don't forward it to next node
      {[], []};
    reject ->
      % message was rejected, don't forward it to next node
      {[], [Msg]}
  end;

%% selection operation: stop on first accept/consume
process_message(Msg, [{select, SelectPipes} | Rest] = _Pipe) ->
  % pass the message through select pipelines
  {Accepted, Rejected} = select(Msg, SelectPipes),
  % each message that was output from select() is passed down the pipe
  Processed = [process_message(A, Rest) || A <- Accepted],
  Acc = [A || {AL,_RL} <- Processed, A <- AL],
  Rej = [R || {_AL,RL} <- Processed, R <- RL],
  {Acc, Rejected ++ Rej};

%% tee operation: pass the message to all the pipelines
process_message(Msg, [{tee, TeePipes} | Rest] = _Pipe) ->
  % pass the message down each of the tee subpipes
  Processed = [process_message(Msg, TP) || TP <- TeePipes],
  % split the results to two (deep) lists
  AcceptedDeep = [AL || {AL,_RL} <- Processed],
  RejectedDeep = [RL || {_AL,RL} <- Processed],
  % whatever was accepted in any of tee subpipes is passed down the pipe
  RestResultDeep = [process_message(A, Rest) || AL <- AcceptedDeep, A <- AL],
  Accepted = [A || {AL,_RL} <- RestResultDeep, A <- AL],
  Rejected = [R || RL <- RejectedDeep, R <- RL] ++
             [R || {_AL,RL} <- RestResultDeep, R <- RL],
  {Accepted, Rejected}.

%% @doc Pass message through selection pipeline.
%%   Processing stops on first accept.
%%
%% @spec select(message(), [pipeline()]) ->
%%   {Accepted :: [message()], Rejected :: [message()]}

-spec select(message(), [pipeline()]) ->
  {Accepted :: [message()], Rejected :: [message()]}.

select(Msg, [] = _Pipes) ->
  {[], [Msg]};
select(Msg, [P | Rest] = _Pipes) ->
  {Accepted, Rejected} = process_message(Msg, P),
  % only rejected messages are passed down select subpipe
  Processed = [select(R, Rest) || R <- Rejected],
  ProcessedAccepted = [A || {AL,_RL} <- Processed, A <- AL],
  ProcessedRejected = [R || {_AL,RL} <- Processed, R <- RL],
  {Accepted ++ ProcessedAccepted, ProcessedRejected}.

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% subscription management
%%%---------------------------------------------------------------------------

%%----------------------------------------------------------
%% subscribe {{{

%% @doc Subscribe to the stream.
%%
%% @spec subscribe(pipeline_handle()) ->
%%   ok

-spec subscribe(pipeline_handle()) ->
  ok.

subscribe(PipePid) ->
  subscribe(PipePid, self()).

%% @doc Subscribe specified process to the stream.
%%   Function intended for {@link streem_registry}.
%%
%% @spec subscribe(pipeline_handle(), pid()) ->
%%   ok

-spec subscribe(pipeline_handle(), pid()) ->
  ok.

subscribe(PipePid, Subscriber) ->
  % TODO: this should be moved to streem_registry:subscribe(Name), so the
  % function may be dropped
  gen_server:call(PipePid, {subscribe, Subscriber}).

%% }}}
%%----------------------------------------------------------
%% unsubscribe {{{

%% @doc Unsubscribe from the stream.
%%
%% @spec unsubscribe(pipeline_handle()) ->
%%   ok

-spec unsubscribe(pipeline_handle()) ->
  ok.

unsubscribe(PipePid) ->
  unsubscribe(PipePid, self()).

%% @doc Unsubscribe specified process from the stream.
%%   Function intended for {@link streem_registry}.
%%
%% @spec unsubscribe(pipeline_handle(), pid()) ->
%%   ok

-spec unsubscribe(pipeline_handle(), pid()) ->
  ok.

unsubscribe(PipePid, Subscriber) ->
  % TODO: this should be moved to streem_registry:subscribe(Name), so the
  % function may be dropped
  gen_server:call(PipePid, {unsubscribe, Subscriber}).

%% @doc Send messages to all subscribers.
%%   The function will take care of stripping metadata from messages.
%%
%% @see send_async/2
%% @spec broadcast([message()], #state{}) ->
%%   ok

-spec broadcast([message()], #state{}) ->
  ok.

broadcast(Messages, State) ->
  [send_async(S, Msg) ||
    S <- State#state.direct_subscribers,   {Msg,_Meta} <- Messages],
  [send_async(S, Msg) ||
    S <- State#state.external_subscribers, {Msg,_Meta} <- Messages],
  ok.

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% starting process
%%%---------------------------------------------------------------------------

%% @doc Start a process that executes specified query.
%%
%% @spec start(pipeline_definition()) ->
%%   {ok, pid()} | {error, Reason}

-spec start(pipeline_definition()) ->
  {ok, pid()} | {error, term()}.

start(NodeDefs) ->
  gen_server:start(?MODULE, NodeDefs, []).

%% @doc Start (and link) a process that executes specified query.
%%
%% @spec start_link(pipeline_definition()) ->
%%   {ok, pid()} | {error, Reason}

-spec start_link(pipeline_definition()) ->
  {ok, pid()} | {error, term()}.

start_link(NodeDefs) ->
  gen_server:start_link(?MODULE, NodeDefs, []).

%% @doc Stop (gracefully) a process that executes specified query.
%%
%% @spec stop(pid()) ->
%%   ok

stop(PipePid) ->
  gen_server:call(PipePid, {stop}).

%%%---------------------------------------------------------------------------
%%% gen_server callbacks
%%%---------------------------------------------------------------------------

%%----------------------------------------------------------
%% init/cleanup {{{

%% @private
%% @doc Process initialization.

init(NodeDefs) ->
  Pipe = init_pipeline(NodeDefs),
  {ok, #state{pipe = Pipe}}.

%% @private
%% @doc Process cleanup.

terminate(_Reason, _State = #state{pipe = Pipe}) ->
  terminate_pipeline(Pipe).

%% }}}
%%----------------------------------------------------------
%% communication with process {{{

%% @private
%% @doc Handle {@link gen_server:call/2}.

%% message submission (synchronous)
handle_call({message, Msg} = _Request, _From, State = #state{pipe = Pipe}) ->
  {Accepted, _Rejected} = process_message({Msg, []}, Pipe),
  % TODO: do something with the `_Rejected'
  broadcast(Accepted, State),
  {reply, ok, State};

%% subscribe request
handle_call({subscribe, Pid} = _Request, _From,
            State = #state{direct_subscribers = Subscribers})
            when is_pid(Pid) ->
  NewSubscribers = monitor_subscriber(Pid, Subscribers),
  {reply, ok, State#state{direct_subscribers = NewSubscribers}};

%% stop the process
handle_call({stop} = _Request, _From, State = #state{}) ->
  {stop, normal, ok, State};

%% unsubscribe request
handle_call({unsubscribe, Pid} = _Request, _From,
            State = #state{direct_subscribers = Subscribers})
            when is_pid(Pid) ->
  NewSubscribers = demonitor_subscriber(Pid, Subscribers),
  {reply, ok, State#state{direct_subscribers = NewSubscribers}};

%% unknown call -- ignore
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%% @private
%% @doc Handle {@link gen_server:cast/2}.

%% unknown cast -- ignore
handle_cast(_Request, State) ->
  {noreply, State}. % ignore unknown calls

%% @private
%% @doc Handle incoming messages.

%% message submission (asynchronous)
handle_info({message, Msg} = _Message, State = #state{pipe = Pipe}) ->
  {Accepted, _Rejected} = process_message({Msg, []}, Pipe),
  % TODO: do something with the `_Rejected'
  broadcast(Accepted, State),
  {noreply, State};

%% external subscriptions
handle_info({subscribers, Subscribers} = _Message, State = #state{}) ->
  % simply replace with new ones
  {noreply, State#state{external_subscribers = Subscribers}};

%% (direct) subscriber is down, but hasn't unsubscribed; unsubscribe him
handle_info({'DOWN', _Ref, process, Pid, _Reason} = _Message,
            State = #state{direct_subscribers = Subscribers}) ->
  NewSubscribers = demonitor_subscriber(Pid, Subscribers),
  {noreply, State#state{direct_subscribers = NewSubscribers}};

%% unknown message -- ignore
handle_info(_Message, State) ->
  {noreply, State}.

%% }}}
%%----------------------------------------------------------
%% code change {{{

%% @private
%% @doc Handle code change notification.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%% }}}
%%----------------------------------------------------------
%% monitoring helper functions {{{

%% @doc Start monitoring process that subscribed this stream.
%%   Mainly for completeness (compare {@link demonitor_subscriber/2}).
%%
%% @spec monitor_subscriber(pid(), [{pid(),reference()}]) ->
%%   [{pid(),reference()}]

-spec monitor_subscriber(pid(), [{pid(),reference()}]) ->
  [{pid(),reference()}].

monitor_subscriber(Pid, Subscribers) ->
  Ref = erlang:monitor(process, Pid),
  [{Pid,Ref} | Subscribers].

%% @doc Stop monitoring specified process.
%%
%% @spec demonitor_subscriber(pid(), [{pid(),reference()}]) ->
%%   [{pid(),reference()}]

-spec demonitor_subscriber(pid(), [{pid(),reference()}]) ->
  [{pid(),reference()}].

demonitor_subscriber(_SubPid, [] = _Subscribers) ->
  [];
demonitor_subscriber(SubPid, [{SubPid,Ref} | Rest] = _Subscribers) ->
  erlang:demonitor(Ref, [flush]),
  demonitor_subscriber(SubPid, Rest);
demonitor_subscriber(SubPid, [Sub | Rest] = _Subscribers) ->
  % subscriber specification doesn't match; keep this subscriber entry
  [Sub | demonitor_subscriber(SubPid, Rest)].

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
