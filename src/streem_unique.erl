%%%---------------------------------------------------------------------------
%%% @private
%%% @doc
%%%   Generate unique string for use as identifier.
%%%
%%%   <b>NOTE</b>: This module doesn't generate strongly (cryptographically)
%%%   unique identifiers. It's intended for making human-usable identifiers
%%%   for streams, and those identifiers should have little chance for
%%%   collision. Erlang's {@type reference()} would suffice here, but it
%%%   doesn't nicely convert to a short (10 bytes long) binary.
%%%
%%%   For better uniqueness guarantees, use UUID generator.
%%%
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_unique).

%% public API
-export([hex_string/0, generate/0]).

%%%---------------------------------------------------------------------------
%%% public API
%%%---------------------------------------------------------------------------

%% @doc Generate identifier and return it as a hex-string.
%%
%% @spec hex_string() ->
%%   string()

hex_string() ->
  hex(generate()).

%% @doc Generate identifier and return it as a binary.
%%   The identifier is 10 bytes long and should be of similar uniqueness as
%%   {@type reference()}.
%%
%% @spec generate() ->
%%   binary()

generate() ->
  {MS, S, US} = now(),
  RefBin = term_to_binary(make_ref()),
  ToHash = <<MS:32, S:32, US:32, RefBin/binary>>,

  % XXX: this is crypto 2.0 (R14) API
  <<Result:10/binary, _/binary>> = crypto:sha(ToHash),
  Result.

%% @doc Convert binary to hex string.
%%
%% @spec hex(binary()) ->
%%   string()

hex(<<>> = _Data) ->
  [];
hex(<<D1:4, D2:4, Rest/binary>> = _Data) ->
  [h(D1), h(D2) | hex(Rest)].

%% @doc Convert integer representation of hex digit to a character.
%%
%% @spec h(integer()) ->
%%   integer()

h(D) when D <  10 -> $0 + D;
h(D) when D >= 10 -> $A + D - 10.

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
