%%%---------------------------------------------------------------------------
%%% @private
%%% @doc
%%%   SJCP client handlers supervisor.
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_sjcp_client_sup).

-behaviour(supervisor).

%% public API
-export([]).

%% starting process
-export([start_link/0]).

%% supervisor callbacks
-export([init/1]).

%%%---------------------------------------------------------------------------
%%% public API
%%%---------------------------------------------------------------------------


%%%---------------------------------------------------------------------------
%%% starting process
%%%---------------------------------------------------------------------------

%% @doc Start supervisor process.

start_link() ->
  supervisor:start_link(?MODULE, []).

%%%---------------------------------------------------------------------------
%%% supervisor callbacks
%%%---------------------------------------------------------------------------

%% @doc Return tree specification.

init([] = _Args) ->
  Strategy = {simple_one_for_one, 5, 10},
  Children = [
    {undefined,
      {streem_sjcp_client, start_link, []},
      temporary, 1000, worker, [streem_sjcp_client]}
  ],
  {ok, {Strategy, Children}}.

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
