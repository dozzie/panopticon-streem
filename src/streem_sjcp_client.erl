%%%---------------------------------------------------------------------------
%%% @private
%%% @doc
%%%   SJCP client connection handler.
%%%
%%% @end
%%%---------------------------------------------------------------------------

-module(streem_sjcp_client).

-behaviour(gen_server).

%% public API
-export([]).

%% starting process
-export([start_link/1]).

%% gen_server callbacks
-export([init/1, terminate/2]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-export([code_change/3]).

-record(state, {sock, subscribers = [], unnamed_queries = []}).

%%%---------------------------------------------------------------------------
%%% public API
%%%---------------------------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% starting process
%%%---------------------------------------------------------------------------

%% @doc Start worker process.
%%
%% @spec start_link(term()) ->
%%   {ok, pid()} | {error, Reason}

-spec start_link(term()) ->
  {ok, pid()} | {error, term()}.

start_link(Socket) ->
  gen_server:start_link(?MODULE, Socket, []).

%%%---------------------------------------------------------------------------
%%% gen_server callbacks
%%%---------------------------------------------------------------------------

%%----------------------------------------------------------
%% init/cleanup {{{

%% @private
%% @doc Process initialization.

init(Socket) ->
  {ok, #state{sock = Socket}}.

%% @private
%% @doc Process cleanup.

terminate(_Reason, _State = #state{sock = Sock, unnamed_queries = Queries}) ->
  gen_tcp:close(Sock),
  [streem_query_manager:stop(QID) || QID <- Queries],
  ok.

%% }}}
%%----------------------------------------------------------
%% communication with process {{{

%% @private
%% @doc Handle {@link gen_server:call/2}.

%% unknown call -- ignore
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%% @private
%% @doc Handle {@link gen_server:cast/2}.

%% unknown cast -- ignore
handle_cast(_Request, State) ->
  {noreply, State}. % ignore unknown calls

%% @private
%% @doc Handle incoming messages.

%% message from one of the pipelines
handle_info({message, Msg} = _Message, State = #state{sock = Sock}) ->
  {ok, Line} = streem_json:encode([{message, Msg}]),
  gen_tcp:send(Sock, [Line, "\n"]),
  {noreply, State};

%% changed subscribers list
handle_info({subscribers, Subscribers} = _Message, State = #state{}) ->
  {noreply, State#state{subscribers = Subscribers}};

%% JSON line from client
handle_info({tcp, Sock, Line} = _Message, State = #state{sock = Sock}) ->
  case decode(Line) of
    {ok, {submit, Msg}} ->
      [streem_pipeline:send_async(P, Msg) || P <- State#state.subscribers],
      ok = reply(Sock, ok, <<"message sent">>),
      {noreply, State};

    {ok, {spidle, Query, Names}} ->
      ListNames = [binary_to_list(N) || N <- Names],
      case streem_query_manager:run(binary_to_list(Query), ListNames) of
        {ok, {QID, Pid}} ->
          ok = reply(Sock, ok, ["query started: ", QID]),
          case ListNames of
            [] ->
              NewUnnamedQueries = [QID | State#state.unnamed_queries],
              link(Pid), % kill the query on error
              streem_registry:subscribe(QID), % subscribe the query
              {noreply, State#state{unnamed_queries = NewUnnamedQueries}};
            _Any ->
              {noreply, State}
          end;
        {error, _Reason} ->
          % TODO: be more specific about the error
          ok = reply(Sock, error, <<"error in query">>),
          {noreply, State}
      end;

    {ok, {register, Channel}} ->
      case streem_registry:register([binary_to_list(Channel)]) of
        {ok, Subscribers} ->
          ok = reply(Sock, ok, <<"channel registered">>),
          {noreply, State#state{subscribers = Subscribers}};
        {error, _Reason} ->
          ok = reply(Sock, error, <<"bad channel name">>),
          {noreply, State}
      end;

    {ok, {unregister, Channel}} ->
      {ok, Subscribers} = streem_registry:unregister(binary_to_list(Channel)),
      ok = reply(Sock, ok, <<"channel unregistered">>),
      {noreply, State#state{subscribers = Subscribers}};

    {ok, {subscribe, Channel}} ->
      case streem_registry:subscribe(binary_to_list(Channel)) of
        ok ->
          ok = reply(Sock, ok, <<"subscribed to ", Channel/binary>>);
        {error, _Reason} ->
          ok = reply(Sock, ok, <<"bad channel name">>)
      end,
      {noreply, State};

    {ok, {unsubscribe, Channel}} ->
      ok = streem_registry:unsubscribe(binary_to_list(Channel)),
      ok = reply(Sock, ok, <<"unsubscribed from ", Channel/binary>>),
      {noreply, State};

    {error, _Reason} ->
      ok = reply(Sock, error, <<"unrecognized command">>),
      {noreply, State}
  end;

%% connection closed
handle_info({tcp_closed, Sock} = _Message, State = #state{sock = Sock}) ->
  {stop, normal, State};

%% connection abruptly closed
handle_info({tcp_error, Sock, Reason} = _Message,
            State = #state{sock = Sock}) ->
  {stop, {tcp_error, Reason}, State};

%% unknown message -- ignore
handle_info(_Message, State) ->
  {noreply, State}.

%% }}}
%%----------------------------------------------------------
%% code change {{{

%% @private
%% @doc Handle code change notification.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%% }}}
%%----------------------------------------------------------
%% SJCP protocol {{{

%% @doc Send reply to client through TCP socket.
%%
%% @spec reply(term(), atom() | string() | binary(), iolist()) ->
%%   ok | {error, term()}

-spec reply(term(), atom() | string() | binary(), iolist() | binary()) ->
  ok | {error, term()}.

reply(Sock, Status, Message) when is_list(Message) ->
  reply(Sock, Status, iolist_to_binary(Message));

% dialyzer: string is never used at the moment
%reply(Sock, Status, Message) when is_list(Status) ->
%  reply(Sock, list_to_binary(Status), Message);

reply(Sock, Status, Message) ->
  {ok, JSON} = streem_json:encode([{status, Status}, {message, Message}]),
  gen_tcp:send(Sock, [JSON, "\n"]).

%% @doc Decode client's request.

-spec decode(binary()) ->
    {ok, {submit, term()}}
  | {ok, {spidle, binary(), [binary()]}}
  | {ok, {register, binary()}}
  | {ok, {unregister, binary()}}
  | {ok, {subscribe, binary()}}
  | {ok, {unsubscribe, binary()}}
  | {error, term()}.

decode(Line) ->
  case streem_json:decode(Line) of
    {ok, [{_,_} | _] = Hash} ->
      case operation(Hash) of
        {ok, {spidle, Query}} ->
          % a little more to do here
          Names = proplists:get_value(<<"names">>, Hash, []),
          {ok, {spidle, Query, Names}};
        {ok, Operation} -> {ok, Operation};
        undefined -> {error, bad_request}
      end;
    {ok, _Any} ->
      {error, bad_request};
    {error, _Reason} = Error ->
      Error
  end.

%% @doc Find the first operation definition in client's request.

-spec operation(list()) ->
    {ok, {submit, term()}}
  | {ok, {spidle, binary()}}
  | {ok, {register, binary()}}
  | {ok, {unregister, binary()}}
  | {ok, {subscribe, binary()}}
  | {ok, {unsubscribe, binary()}}
  | undefined.

operation([] = _Hash) ->
  undefined;

operation([{<<"submit">>, Message} | _Rest] = _Hash)
when is_list(Message) ->
  {ok, {submit, Message}};

operation([{<<"query">>, Query} | _Rest] = _Hash)
when is_binary(Query) ->
  {ok, {spidle, Query}};

operation([{<<"register">>, Channel} | _Rest] = _Hash)
when is_binary(Channel) ->
  {ok, {register, Channel}};

operation([{<<"unregister">>, Channel} | _Rest] = _Hash)
when is_binary(Channel) ->
  {ok, {unregister, Channel}};

operation([{<<"subscribe">>, Channel} | _Rest] = _Hash)
when is_binary(Channel) ->
  {ok, {subscribe, Channel}};

operation([{<<"unsubscribe">>, Channel} | _Rest] = _Hash)
when is_binary(Channel) ->
  {ok, {unsubscribe, Channel}};

operation([_Any | Rest] = _Hash) ->
  operation(Rest).

%% }}}
%%----------------------------------------------------------

%%%---------------------------------------------------------------------------
%%% vim:ft=erlang:foldmethod=marker
